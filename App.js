import React from "react";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator, DrawerItems } from "react-navigation-drawer";
import {
  View,
  StyleSheet,
  SafeAreaView,
  Image,
  TextInput,
  TouchableOpacity,
  Text,
  FlatList,
  Dimensions,
} from "react-native";
import { setNavigator } from "./src/navigationRef";
import { Provider as AuthProvider } from "./src/context/AuthContext";
import resolveAuthScreen from "./src/screens/resolveAuthScreen";

//Welcome Component
import Welcome from "./src/screens/Welcome/Welcome";

//Login Component
import Login from "./src/screens/Auth/Login/Login";

//Register Components
import FirstRegister from "./src/screens/Auth/Register/FirstRegister/FirstRegister";
import SecondRegister from "./src/screens/Auth/Register/SecondRegister/SecondRegister";
import ThirdRegister from "./src/screens/Auth/Register/ThirdRegister/ThirdRegister";
import Review from "./src/screens/Auth/Register/Review/Review";

//Forget Password Components
import ForgetPassword from "./src/screens/Auth/ForgetPassword/ForgetPassword";
import CheckCode from "./src/screens/Auth/ForgetPassword/CheckCode";
import NewPass from "./src/screens/Auth/ForgetPassword/NewPass";

//Home Components
import Home from "./src/screens/Home/Home";
import pharmacies from "./src/screens/Home/pharmacies";
import workinghours from "./src/screens/Home/workinghours";

//Orders Components
import OrdersPending from "./src/screens/orders/ordersPending";
import OrdersConfirm from "./src/screens/orders/ordersConfirm";

//Reservations Components
import ReservationsPending from "./src/screens/reservation/reservationsPending";
import ReservationsConfirm from "./src/screens/reservation/reservationsConfirm";

//Profile Components
import Profile from "./src/screens/Profile/profile";
import profilepharmacieds from "./src/screens/Profile/profilepharmacieds";
import EditProfile from "./src/screens/Profile/editprofile";

//saved documents Components
import SavedFolders from "./src/screens/Saved/SavedFolders";
import AddSaved from "./src/screens/Saved/AddSaved";
import SavedList from "./src/screens/Saved/SavedList";

//Chat Components
import Massages from "./src/screens/Massages/massages";
import Chat from "./src/screens/Chat/Chat";

import report from "./src/screens/report/report";

import CustomDrawerComponent from "./src/components/Drawer";
import Notifications from "./src/components/notifications";

const switchNavigator = createSwitchNavigator({
  resolveAuthScreen: resolveAuthScreen,
  loginFlow: createStackNavigator(
    {
      Welcome,
      Login,
      FirstRegister,
      SecondRegister,
      ThirdRegister,
      Review,
      ForgetPassword,
      CheckCode,
      NewPass,
    },
    {
      initialRouteName: "Welcome",
    }
  ),

  onmainFlow: createDrawerNavigator(
    {
      Home,
      OrdersPending,
      OrdersConfirm,
      ReservationsPending,
      ReservationsConfirm,

      Profile,
      profilepharmacieds,
      EditProfile,

      pharmacies,
      workinghours,

      Massages,
      Chat,

      report,

      SavedFolders,
      AddSaved,
      SavedList,
    },
    {
      initialRouteName: "Home",
      contentComponent: CustomDrawerComponent,
      drawerPosition: "left",
      drawerOpenRoute: "DrawerOpen",
      drawerCloseRoute: "DrawerClose",
      drawerToggleRoute: "DrawerToggle",
    }
  ),
});

const Root = createAppContainer(switchNavigator);

function App({ navigation }) {
  return (
    <AuthProvider>
      <Root
        ref={(navigator) => {
          setNavigator(navigator);
        }}
      />
    </AuthProvider>
  );
}

export default App;

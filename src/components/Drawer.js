import React, { useState, useEffect, useContext } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  Image,
  ActivityIndicator,
  Dimensions,
} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import profilepc from "../../assets/images/profilepc.png";
import { Context as AuthContext } from "../context/AuthContext";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import * as OpenAnything from "react-native-openanything";

const { width, height } = Dimensions.get("window");

const CustomDrawerComponent = ({ navigation }) => {
  const [role, setRole] = useState(0);
  const [token, setToken] = useState("");
  const [name, setName] = useState("");
  const [profile_image, setImage] = useState("");
  const [settings, setSettings] = useState(false);

  const { signout } = useContext(AuthContext);

  useEffect(() => {
    getToken();
  });

  const getImage = async () => {
    const profile_image = JSON.parse(
      await AsyncStorage.getItem("profile_image")
    );
    const name = JSON.parse(await AsyncStorage.getItem("name"));
    setImage(profile_image);
    setName(name);
  };

  const getToken = async () => {
    const token = JSON.parse(await AsyncStorage.getItem("token"));
    const name = JSON.parse(await AsyncStorage.getItem("name"));
    const role = Number(await AsyncStorage.getItem("role"));
    const profile_image = JSON.parse(
      await AsyncStorage.getItem("profile_image")
    );

    if (token) {
      setToken(token);
      setName(name);
      setRole(role);
      setImage(profile_image);
    } else {
      console.log("ERROR GET TOKEN");
    }
  };

  const infoDoctor = () => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/Doctor/show?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        { token }
      )
      .then((response) => {
        console.log("RESPONSE INFO", response);
      })
      .catch((error) => {
        console.log("ERROR: ", error);
      });
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ backgroundColor: "#004FFE" }}>
        <TouchableOpacity onPress={() => navigation.closeDrawer()}>
          <FontAwesome5
            style={{ padding: 20 }}
            active
            size={25}
            name={"align-justify"}
            color="white"
          />
        </TouchableOpacity>

        <View style={{ padding: 10, flexDirection: "row" }}>
          <Image
            source={{ uri: profile_image }}
            style={styles.profilePicture}
            resizeMode="contain"
            PlaceholderContent={<ActivityIndicator />}
          />
          <View style={{ alignSelf: "center" }}>
            <Text style={{ fontSize: 17, color: "#fff", fontWeight: "bold" }}>
              {name}
            </Text>
            {role === 3 && (
              <Text style={{ fontSize: 17, color: "#fff", fontWeight: "bold" }}>
                Brand Name
              </Text>
            )}
            <Text
              style={{ fontSize: 17, color: "#fff", fontWeight: "bold" }}
            ></Text>
          </View>
        </View>

        <TouchableOpacity
          onPress={() => navigation.navigate("Massages")}
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            borderBottomWidth: 0.3,
            borderTopWidth: 0.3,
            borderColor: "#fff",
          }}
        >
          <Text style={{ color: "#fff", fontSize: 17 }}>Massages</Text>

          <FontAwesome5
            style={{ padding: 10, marginLeft: "46%" }}
            active
            size={30}
            name={"angle-right"}
            color="white"
          />
        </TouchableOpacity>
        {(role === 2 || role === 3) && (
          <TouchableOpacity
            onPress={() => navigation.navigate("workinghours")}
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderBottomWidth: 0.3,
              borderColor: "#fff",
            }}
          >
            <Text style={{ color: "#fff", fontSize: 17 }}>Working Hours</Text>

            <FontAwesome5
              style={{ padding: 10, marginLeft: "33%" }}
              active
              size={30}
              name={"angle-right"}
              color="white"
            />
          </TouchableOpacity>
        )}

        <View style={{ height: 30, backgroundColor: "#004FFE" }} />
      </View>

      <View style={{ backgroundColor: "#fff", flex: 1 }}>
        <View style={{ padding: 20 }}>
          <TouchableOpacity
            style={{ padding: 10 }}
            onPress={() => navigation.navigate("EditProfile")}
          >
            <Text
              style={{ fontSize: 17, color: "#707070", fontWeight: "bold" }}
            >
              Edit Profile
            </Text>
          </TouchableOpacity>

          {role === 4 && (
            <TouchableOpacity
              style={{ padding: 10 }}
              onPress={() => navigation.navigate("OrdersPending")}
            >
              <Text
                style={{ fontSize: 17, color: "#707070", fontWeight: "bold" }}
              >
                My Orders
              </Text>
            </TouchableOpacity>
          )}
          <TouchableOpacity
            style={{ padding: 10 }}
            onPress={() => navigation.navigate("SavedFolders")}
          >
            <Text
              style={{ fontSize: 17, color: "#707070", fontWeight: "bold" }}
            >
              Saved
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{ padding: 10 }}
            onPress={() => navigation.navigate("ReservationsPending")}
          >
            <Text
              style={{ fontSize: 17, color: "#707070", fontWeight: "bold" }}
            >
              Reservations
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{ padding: 10 }}
            onPress={() => setSettings(!settings)}
          >
            <Text
              style={{ fontSize: 17, color: "#707070", fontWeight: "bold" }}
            >
              Settings
            </Text>
          </TouchableOpacity>
          {settings && (
            <View style={{ paddingLeft: "10%" }}>
              {(role === 2 || role === 3) && (
                <TouchableOpacity
                  onPress={() =>
                    OpenAnything.Pdf(
                      "https://www.teeth-jo.com/pdf/contract.pdf"
                    )
                  }
                >
                  <Text
                    style={{
                      color: "#2065FE",
                      fontSize: 13,
                      marginBottom: 5,
                    }}
                  >
                    <FontAwesome5
                      active
                      size={7}
                      name={"circle"}
                      color="#707070"
                      solid
                    />
                    {"  "}
                    Contract
                  </Text>
                </TouchableOpacity>
              )}
              <TouchableOpacity
                onPress={() =>
                  OpenAnything.Pdf("https://www.teeth-jo.com/pdf/policy.pdf")
                }
              >
                <Text
                  style={{ color: "#305FDF", fontSize: 13, marginBottom: 5 }}
                >
                  <FontAwesome5
                    active
                    size={7}
                    name={"circle"}
                    color="#707070"
                    solid
                  />
                  {"  "}
                  Terms and Conditions
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={signout}>
                <Text
                  style={{
                    fontSize: 13,
                    color: "#305FDF",
                    marginBottom: 5,
                  }}
                >
                  <FontAwesome5
                    active
                    size={7}
                    name={"circle"}
                    color="#707070"
                    solid
                  />
                  {"  "}
                  Log Out
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
    </SafeAreaView>
  );
};

export default CustomDrawerComponent;

const styles = StyleSheet.create({
  profilePicture: {
    width: height / 8,
    height: height / 8,
    resizeMode: "cover",
    borderRadius: height / 6.5,
    marginRight: 10,
  },
});

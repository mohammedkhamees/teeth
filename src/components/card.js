import React from 'react';
import {
	View,
	StyleSheet,
	ActivityIndicator,
	Text,
	Dimensions,
	Image,
	TouchableOpacity,
} from 'react-native';
import {
	sliderItemWidth,
	sliderItemHorizontalMargin,
	slideWidth,
} from './style';

const { width, height } = Dimensions.get('window');
// orientation must fixed
const SCREEN_WIDTH = width < height ? width : height;
// const SCREEN_HEIGHT = width < height ? height : width;
const isSmallDevice = SCREEN_WIDTH <= 400;

const Card = ({ title }) => (
	<View
		style={{
			width: sliderItemWidth,
			height: 200,
			paddingHorizontal: sliderItemHorizontalMargin,
			alignItems: 'center',
			justifyContent: 'center',
		}}
	>
		<View style={{ padding: 5, alignItems: 'center', marginTop: '100%' }}>
			<TouchableOpacity
				style={{
					width: 125,
					alignItems: 'center',
					height: 170,
					borderColor: 'blue',
					borderWidth: 1,
					borderTopStartRadius: 80,
					borderTopRightRadius: 80,
					borderBottomEndRadius: 10,
					borderBottomLeftRadius: 10,
				}}
			>
				<Image
					source={require('./profilepc.png')}
					style={styles.profilePictureDoctors}
					resizeMode="contain"
					PlaceholderContent={<ActivityIndicator />}
				/>
			</TouchableOpacity>
			<TouchableOpacity
				style={{
					marginTop: 10,
					width: 125,
					alignItems: 'center',
					height: 170,
					borderColor: 'blue',
					borderWidth: 1,
					borderTopStartRadius: 80,
					borderTopRightRadius: 80,
					borderBottomEndRadius: 10,
					borderBottomLeftRadius: 10,
				}}
			>
				<Image
					source={require('./profilepc.png')}
					style={styles.profilePictureDoctors}
					resizeMode="contain"
					PlaceholderContent={<ActivityIndicator />}
				/>
			</TouchableOpacity>
		</View>
	</View>
);

export default Card;

const styles = StyleSheet.create({
	inputBox: {
		width: '90%',
		height: 35,
		borderBottomColor: '#6B87D6',
		backgroundColor: '#fff',
		color: 'blue',
		fontSize: 15,
		fontFamily: 'DINNextMedium',
		borderRadius: 35,
	},
	profilePictureWrapper: {
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 1,
	},
	profilePicture: {
		width: height / 4.1,
		height: height / 4.1,
		resizeMode: 'cover',
		borderRadius: height / 6,
	},
	profilePictureDoctors: {
		width: height / 6,
		height: height / 6,
		resizeMode: 'cover',
		borderRadius: height / 6,
		padding: 1,
	},
	imagesGroup: {
		flexWrap: 'wrap',
		margin: 5,
		alignItems: 'center',
		justifyContent: 'center',
	},
	text: {
		fontSize: 13,
		color: '#fff',
		fontFamily: 'DINNextMedium',
		marginBottom: 10,
	},
});

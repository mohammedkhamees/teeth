import React, { useState, useEffect, useRef } from "react";
import { Text, View, Button } from "react-native";
import * as Notification from "expo-notifications";
import * as Permissions from "expo-permissions";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";

// Notification options
Notification.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: false,
  }),
});

const schedulePushNotification = async (content) => {
  await Notification.scheduleNotificationAsync({
    content,
    trigger: { seconds: 2 },
  });
};

const Notifications = () => {
  const [tokenAuth, setToken] = useState("");
  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();

  const getNotificationContent = () => {
    axios
      .get(`https://nextstageksa.com/clinic/api/users/notifications`, {
        headers: {
          Authorization: `Bearer ${tokenAuth}`,
        },
      })
      .then(async (res) => {
        content = {
          title: res.data.data[0].notification + " title",
          body: res.data.data[0].notification + " body",
          data: { data: "Data goes here" },
        };

        await schedulePushNotification(content);
      })
      .catch((err) => console.log("getNotificationContent err", err));
  };

  useEffect(() => {
    getToken();

    // registerForPushNotification()
    //   .then((token) => console.log(token))
    //   .catch((err) => console.log("err", err));

    notificationListener.current = Notification.addNotificationReceivedListener(
      (notification) => {
        setNotification(notification);
      }
    );

    responseListener.current =
      Notification.addNotificationResponseReceivedListener((response) => {
        console.log(response);
      });

    return () => {
      Notification.removeNotificationSubscription(notificationListener.current);
      Notification.removeNotificationSubscription(responseListener.current);
    };
  }, []);

  const getToken = async () => {
    const token = JSON.parse(await AsyncStorage.getItem("token"));
    if (token) {
      setToken(token);
    } else {
      console.log("ERROR GET TOKEN");
    }
  };

  async function registerForPushNotification() {
    const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    if (status != "granted") {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    }
    if (status !== "granted") {
      alert("Failed to get push token for push notification!");
      return;
    }
    token = (await Notification.getExpoPushTokenAsync()).data;
    return token;
  }

  return (
    <View style={{ position: "absolute", top: "50%" }}>
      <Button title="Press Notification" onPress={getNotificationContent} />
    </View>
  );
};

export default Notifications;

import createDataContext from "./createDataContext";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { navigate } from "../navigationRef";

const authReducer = (state, action) => {
  switch (action.type) {
    case "add_error":
      return { ...state, errorMessage: action.payload };
    case "signup":
      return { ...state, token: action.payload };
    case "signin":
      return { ...state, token: action.payload };
    case "signout":
      return { token: null, errorMessage: "" };
    default:
      return state;
  }
};

const tryLocalSignin = (dispatch) => async () => {
  const token = await AsyncStorage.getItem("token");
  if (token) {
    dispatch({ type: "signin", payload: token });
    navigate("Home");
  } else {
    navigate("Welcome");
  }
};

const signup =
  (dispatch) =>
  async ({
    name,
    phone,
    email,
    password,
    configPass,
    profile_image,
    role_id,
    Gender,
    Age,
    lng,
    lat,
    key,
    id_city,
    key_District,
    brand,
    specialized,
    resident,
    comm_rec,
    image,
    national_id,
  }) => {
    try {
      if (role_id === 4) {
        const response = await axios.post(
          "https://nextstageksa.com/clinic/api/registers",
          {
            name,
            phone,
            email,
            password,
            configPass,
            profile_image,
            role_id,
            Gender,
            Age,
            lng,
            lat,
            key,
            id_city,
            key_District,
          }
        );

        if (response.data.Data.User.api_token) {
          await AsyncStorage.setItem(
            "name",
            JSON.stringify(response.data.Data.User.name)
          );
          await AsyncStorage.setItem(
            "role",
            JSON.stringify(response.data.Data.User.role_id)
          );
          await AsyncStorage.setItem(
            "token",
            JSON.stringify(response.data.Data.User.api_token)
          );
          await AsyncStorage.setItem(
            "email",
            JSON.stringify(response.data.Data.User.email)
          );
          await AsyncStorage.setItem(
            "phone",
            JSON.stringify(response.data.Data.User.phone)
          );
          await AsyncStorage.setItem(
            "Gender",
            JSON.stringify(response.data.Data.User.Gender)
          );
          await AsyncStorage.setItem(
            "Age",
            JSON.stringify(response.data.Data.User.Age)
          );
          await AsyncStorage.setItem(
            "profile_image",
            JSON.stringify(response.data.Data.User.profile_image)
          );
          dispatch({
            type: "signin",
            payload: response.data.Data.User.api_token,
          });
          navigate("Home");
        }
      } else {
        const response = await axios.post(
          "https://nextstageksa.com/clinic/api/registers",
          {
            name,
            phone,
            email,
            password,
            configPass,
            profile_image,
            role_id,
            Gender,
            Age,
            lng,
            lat,
            key,
            id_city,
            key_District,
            brand,
            specialized,
            resident,
            comm_rec,
            image,
            national_id,
          }
        );
        navigate("Review");
      }
    } catch (err) {
      console.log("err", err.message);
    }
  };

const signin =
  (dispatch) =>
  async ({ email, password, key }) => {
    try {
      const response = await axios.post(
        `https://nextstageksa.com/clinic/api/login`,
        { email, password, key }
      );

      if (response.data.User.User.api_token) {
        await AsyncStorage.setItem(
          "name",
          JSON.stringify(response.data.User.User.name)
        );
        await AsyncStorage.setItem(
          "role",
          JSON.stringify(response.data.User.User.role_id)
        );
        await AsyncStorage.setItem(
          "token",
          JSON.stringify(response.data.User.User.api_token)
        );
        await AsyncStorage.setItem(
          "email",
          JSON.stringify(response.data.User.User.email)
        );
        await AsyncStorage.setItem(
          "phone",
          JSON.stringify(response.data.User.User.phone)
        );
        await AsyncStorage.setItem(
          "Gender",
          JSON.stringify(response.data.User.User.Gender)
        );
        await AsyncStorage.setItem(
          "Age",
          JSON.stringify(response.data.User.User.Age)
        );
        await AsyncStorage.setItem(
          "profile_image",
          JSON.stringify(response.data.User.User.profile_image)
        );
        dispatch({
          type: "signin",
          payload: response.data.User.User.api_token,
        });
        navigate("Home");
      } else if (response.data.erorr.email) {
        dispatch({ type: "add_error", payload: response.data.erorr.email });
      } else if (response.data.erorr.password) {
        dispatch({ type: "add_error", payload: response.data.erorr.password });
      } else {
        console.log("good");
      }
    } catch (err) {
      alert("wrong email or password");
    }
  };

const signout = (dispatch) => async () => {
  await AsyncStorage.removeItem("name");
  await AsyncStorage.removeItem("role");
  await AsyncStorage.removeItem("token");
  dispatch({ type: "signout" });
  navigate("Login");
};

export const { Provider, Context } = createDataContext(
  authReducer,
  { signin, signout, signup, tryLocalSignin },
  { token: null, errorMessage: "" }
);

import React, { Component } from 'react';
import { createAppContainer } from 'react-navigation';

import MainDrawer from './homeStack';

////////////////////////////////////////////////////////////////////////////////

const AppContainer = createAppContainer(MainDrawer);
export default class App extends Component {
	render() {
		return <AppContainer />;
	}
}

import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Dimensions,
  Alert,
  ScrollView,
  Text,
  TouchableWithoutFeedback,
} from "react-native";
import i18n from "i18n-js";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Root } from "native-base";
///////////////
import { Image } from "react-native-elements";
import { connect } from "react-redux";

import { NavigationActions, StackActions } from "react-navigation";

import API from "../../services/config/api"; ////////////
///////////////
//import MenuButton from "../components/button/MenuButton";

// screen sizing
const { width, height } = Dimensions.get("window");
// orientation must fixed
const SCREEN_WIDTH = width < height ? width : height;

const isSmallDevice = SCREEN_WIDTH <= 400;
const drawerMarginTop = isSmallDevice ? 5 : 50;

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: "Login" })],
});

class DrawerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////

  goToScreen(screenName) {
    this.props.navigation.navigate(screenName);
    this.props.navigation.closeDrawer();
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////

  UserLogoutFunction = async () => {
    await AsyncStorage.getItem("api_token").then((token) => {
      this.setState({
        api_token: token,
      });
      console.log("AsyncStorage.getItem(api_token)", token);
    });

    const items = ["api_token", "profile_picture", "userName"];

    fetch(API.baseURL + "logout", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.state.api_token,
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.status == 200) {
          AsyncStorage.multiRemove(items);
          this.props.navigation.dispatch(resetAction);
          this.props.logout();
        } else {
          AsyncStorage.multiRemove(items);
          this.props.navigation.dispatch(resetAction);
          this.props.logout();
          Alert.alert(responseJson.message, " ", [
            { text: i18n.t("ok"), onPress: () => console.log("Error") },
          ]);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////

  render() {
    return (
      <Root>
        <ScrollView
          style={{ marginTop: 10, flex: 1, marginTop: drawerMarginTop }}
          contentContainerStyle={{
            alignItems: "center",
            justifyContent: "center",
            flexGrow: 1,
            width: "100%",
            paddingBottom: 30,
            marginBottom: 10,
          }}
          showsVerticalScrollIndicator={false}
        >
          <View
            style={{
              flex: 1,
              width: "100%",
              height: "100%",
              alignItems: "center",
            }}
          >
            {this.props.userIsLoggedIn != false ? (
              <View
                style={{
                  width: "95%",
                  height: 150,
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  alignItems: "center",
                  marginBottom: 20,
                }}
              >
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.props.navigation.navigate("ProfileScreen")
                  }
                >
                  {this.props.profilePicture != null &&
                  this.props.profilePicture != "" ? (
                    <View
                      style={{
                        width: "100%",
                        height: "100%",
                        flexDirection: "row",
                        justifyContent: "flex-start",
                        alignItems: "center",
                      }}
                    >
                      <Image
                        source={{ uri: this.props.profilePicture }}
                        style={{
                          width: 100,
                          height: 100,
                          borderRadius: 50,
                        }}
                        resizeMode="cover"
                      />
                      <Text
                        style={{
                          fontFamily: "DINNextMedium",
                          fontSize: 16,
                          marginLeft: 10,
                        }}
                      >
                        {this.props.userName}
                      </Text>
                    </View>
                  ) : (
                    <View
                      style={{
                        width: "100%",
                        height: "100%",
                        flexDirection: "row",
                        justifyContent: "flex-start",
                        alignItems: "center",
                      }}
                    >
                      <Image
                        source={require("../../assets/images/profileImage.png")}
                        style={{
                          width: 100,
                          height: 100,
                        }}
                        resizeMode="contain"
                      />
                      <Text
                        style={{
                          fontFamily: "DINNextMedium",
                          fontSize: 16,
                          marginLeft: 10,
                        }}
                      >
                        {this.props.userName}
                      </Text>
                    </View>
                  )}
                </TouchableWithoutFeedback>
              </View>
            ) : null}
            <View
              style={{
                width: "100%",
                height: "100%",
                marginTop: this.props.userIsLoggedIn == false ? height / 5 : 0,
              }}
            >
              <TouchableOpacity
                onPress={() => this.goToScreen("Home")}
                style={{ justifyContent: "center", padding: 5 }}
              >
                <Text
                  style={{
                    color: "#333",
                    fontFamily: "DINNextMedium",
                    fontSize: 16,
                    textAlign: "left",
                  }}
                >
                  {i18n.t("mainScreen")}
                </Text>
              </TouchableOpacity>
              {this.props.userIsLoggedIn != false ? (
                <TouchableOpacity
                  onPress={() => this.goToScreen("MyAdvertisements")}
                  style={{ justifyContent: "center", padding: 5 }}
                >
                  <Text
                    style={{
                      color: "#333",
                      fontFamily: "DINNextMedium",
                      fontSize: 16,
                      textAlign: "left",
                    }}
                  >
                    {i18n.t("myAdvertisements")}
                  </Text>
                </TouchableOpacity>
              ) : null}
              {this.props.userIsLoggedIn != false ? (
                <TouchableOpacity
                  onPress={() => this.goToScreen("userBookingRequests")}
                  style={{ justifyContent: "center", padding: 5 }}
                >
                  <Text
                    style={{
                      color: "#333",
                      fontFamily: "DINNextMedium",
                      fontSize: 16,
                      textAlign: "left",
                    }}
                  >
                    {i18n.t("myBookings")}
                  </Text>
                </TouchableOpacity>
              ) : null}

              {this.props.userIsLoggedIn != false ? (
                <TouchableOpacity
                  onPress={() => this.goToScreen("ProviderBookingRequests")}
                  style={{ justifyContent: "center", padding: 5 }}
                >
                  <Text
                    style={{
                      color: "#333",
                      fontFamily: "DINNextMedium",
                      fontSize: 16,
                      textAlign: "left",
                    }}
                  >
                    {i18n.t("bookingRequests")}
                  </Text>
                </TouchableOpacity>
              ) : null}

              {this.props.userIsLoggedIn != false ? (
                <TouchableOpacity
                  onPress={() => this.goToScreen("AddReport")}
                  style={{ justifyContent: "center", padding: 5 }}
                >
                  <Text
                    style={{
                      color: "#333",
                      fontFamily: "DINNextMedium",
                      fontSize: 16,
                      textAlign: "left",
                    }}
                  >
                    {i18n.t("addReport")}
                  </Text>
                </TouchableOpacity>
              ) : null}
              <TouchableOpacity
                onPress={() => this.goToScreen("ContactUs")}
                style={{ justifyContent: "center", padding: 5 }}
              >
                <Text
                  style={{
                    color: "#333",
                    fontFamily: "DINNextMedium",
                    fontSize: 16,
                    textAlign: "left",
                  }}
                >
                  {i18n.t("contactUs")}
                </Text>
              </TouchableOpacity>
              {this.props.userIsLoggedIn == false ? (
                <TouchableOpacity
                  onPress={() => this.props.navigation.dispatch(resetAction)}
                  style={{ justifyContent: "center", padding: 5 }}
                >
                  <Text
                    style={{
                      color: "#333",
                      fontFamily: "DINNextMedium",
                      fontSize: 16,
                      textAlign: "left",
                    }}
                  >
                    {i18n.t("loginToUseAllFeatures")}
                  </Text>
                </TouchableOpacity>
              ) : null}
              <TouchableOpacity
                onPress={() => this.goToScreen("Settings")}
                style={{ justifyContent: "center", padding: 5 }}
              >
                <Text
                  style={{
                    color: "#333",
                    fontFamily: "DINNextMedium",
                    fontSize: 16,
                    textAlign: "left",
                  }}
                >
                  {i18n.t("settings")}
                </Text>
              </TouchableOpacity>

              {this.props.userIsLoggedIn != false ? (
                <TouchableOpacity
                  onPress={() => this.UserLogoutFunction()}
                  style={{
                    justifyContent: "center",
                    padding: 5,
                  }}
                >
                  <Text
                    style={{
                      color: "#333",
                      fontFamily: "DINNextMedium",
                      fontSize: 16,
                      textAlign: "left",
                    }}
                  >
                    {i18n.t("logout")}
                  </Text>
                </TouchableOpacity>
              ) : null}

              <View style={{ height: 20 }} />
            </View>
          </View>
        </ScrollView>
      </Root>
    );
  }
}

function mapStateToProps(state) {
  return {
    userIsLoggedIn: state.userIsLoggedIn,
    profilePicture: state.profilePicture,
    userName: state.userName,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch({ type: "LOGGED_OUT", isLoggedOut: false }),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(DrawerComponent);

import React from 'react';
import { Dimensions, Image, I18nManager, View } from 'react-native';
import i18n from 'i18n-js';
import * as Device from 'expo-device';

import { createBottomTabNavigator, BottomTabBar } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';

import SplashScreen from '../screens/Splash/SplashScreen';
import Login from '../screens/Login/Login';
import Register from '../screens/Login/Register';
import Checkcode from '../screens/Login/Checkcode';
import HomeScreen from '../screens/Home/home';
import FarmsListScreen from '../screens/FarmsList/FarmsListScreen';
import FarmInfoScreen from '../screens/FarmInfo/FarmInfoScreen';
import AddFarmScreen from '../screens/AddFarm/AddFarmScreen';

import FavoritesListScreen from '../screens/FavoritesList/FavoritesListScreen';
import MyAdvertisementsScreen from '../screens/MyAdvertisements/MyAdvertisementsScreen';
import ContactUsScreen from '../screens/ContactUs/ContactUsScreen';
import AddReportScreen from '../screens/AddReport/AddReportScreen';

import AppointmentBookingScreen from '../screens/AppointmentBooking/AppointmentBookingScreen';
import ProviderBookingRequestsScreen from '../screens/ProviderBookingRequests/ProviderBookingRequestsScreen/ProviderBookingRequestsScreen';
import ProviderBookingRequestsInfoScreen from '../screens/ProviderBookingRequests/ProviderBookingRequestsInfoScreen/ProviderBookingRequestsInfoScreen';
import userBookingRequestsScreen from '../screens/UserBookingRequests/UserBookingRequestsScreen/UserBookingRequestsScreen';
import UserBookingRequestsInfoScreen from '../screens/UserBookingRequests/UserBookingRequestsInfoScreen/UserBookingRequestsInfoScreen';

import SettingsScreen from '../screens/Settings/SettingsScreen';
import SetLanguageScreen from '../screens/SetLanguage/SetLanguageScreen';
import PrivacyPolicy from '../screens/PrivacyPolicy/index';

import DrawerComponent from './Drawer';
import getCurrentLocation from '../components/GetCurrentLocation/getCurrentLocation';
import Header from '../components/Header/Header';

import ProfileScreen from '../screens/Profile';
import ChangePassword from '../screens/changepassword/index';

const { width, height } = Dimensions.get('window');
// orientation must fixed
const SCREEN_WIDTH = width < height ? width : height;
// const SCREEN_HEIGHT = width < height ? height : width;
const isSmallDevice = SCREEN_WIDTH <= 400;

//Get Device Model
const deviceModel = Device.modelName;
//console.log("deviceModel_____________", deviceModel);

//Check If Device Has Notch
const isNotch =
	deviceModel === 'iPhone' || //Check for New Models Not Recognized Yet from React Native
	deviceModel === 'iPhone X' ||
	deviceModel === 'iPhone XR' ||
	deviceModel === 'iPhone XS' ||
	deviceModel === 'iPhone XS Max' ||
	deviceModel === 'iPhone 11' ||
	deviceModel === 'iPhone 11 Pro' ||
	deviceModel === 'iPhone 11 Pro Max' ||
	deviceModel === 'iPhone 12 mini' ||
	deviceModel === 'iPhone 12' ||
	deviceModel === 'iPhone 12 Pro' ||
	deviceModel === 'iPhone 12 Pro Max';

//If Device Has Notch Add Margin Bottom -12
const TabBar_Bottom = isNotch ? -12 : 0;

// const TabBarComponent = (props) => <BottomTabBar {...props} />;

const homeStack = createStackNavigator(
	{
		Home: {
			screen: HomeScreen,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						return (
							<Header navigation={navigation} title={i18n.t('mainScreen')} />
						);
					},
					gestureEnabled: false,
				};
			},
		},
		FarmsList: {
			screen: FarmsListScreen,
		},
		FarmInfo: {
			screen: FarmInfoScreen,
		},
	},

	{
		headerMode: 'screen',
		initialRouteName: 'Home',
	},
);

const bookingsStack = createStackNavigator(
	{
		userBookingRequests: {
			screen: userBookingRequestsScreen,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						return (
							<Header navigation={navigation} title={i18n.t('myBookings')} />
						);
					},
				};
			},
		},
		UserBookingRequestsInfo: {
			screen: UserBookingRequestsInfoScreen,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						return (
							<Header
								navigation={navigation}
								title={i18n.t('myBookings')}
								backButton
							/>
						);
					},
				};
			},
		},
	},

	{
		headerMode: 'screen',
		initialRouteName: 'userBookingRequests',
	},
);

const favoritesStack = createStackNavigator(
	{
		FavoritesList: {
			screen: FavoritesListScreen,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						return (
							<Header navigation={navigation} title={i18n.t('favorites')} />
						);
					},
					gestureEnabled: false,
				};
			},
		},
		FarmInfo: {
			screen: FarmInfoScreen,
		},
	},

	{
		headerMode: 'screen',
		initialRouteName: 'FavoritesList',
	},
);

const MyBottomTab = createBottomTabNavigator(
	{
		// Stack: StackHome,

		Home: {
			screen: homeStack,
			navigationOptions: ({ navigation }) => ({
				swipeEnabled: false,
				gestureEnabled: false,
				tabBarLabel: ' ',
				tabBarIcon: ({ focused, tintColor }) => {
					return (
						<Image
							style={{
								width: 30,
								height: 30,
								tintColor: tintColor,
							}}
							source={require('../../assets/icons/home.png')}
						/>
					);
				},
			}),
		},
		bookings: {
			screen: bookingsStack,
			navigationOptions: ({ navigation }) => {
				return {
					swipeEnabled: false,
					gestureEnabled: false,
					tabBarLabel: ' ',

					tabBarIcon: ({ focused, tintColor }) => (
						<View
							style={{
								height: 70,
								width: 70,
								backgroundColor: focused ? '#efefef' : '#efefef',
								borderRadius: 40,
								borderWidth: 3,
								borderColor: focused ? '#92E087' : '#F28421',
								justifyContent: 'center',
								alignItems: 'center',
								marginTop: isNotch ? 0 : -23,
							}}
						>
							<Image
								source={require('../../assets/icons/calendar.png')}
								style={{
									height: 42,
									width: 42,
									tintColor: focused ? '#92E087' : '#F28421',
								}}
								resizeMode="contain"
							/>
						</View>
					),
				};
			},
		},

		Favorites: {
			screen: favoritesStack,
			navigationOptions: ({ navigation }) => ({
				swipeEnabled: false,
				gestureEnabled: false,
				tabBarLabel: ' ',
				tabBarIcon: ({ focused, tintColor }) => {
					return (
						<Image
							style={{
								width: 30,
								height: 30,
								tintColor: tintColor,
							}}
							source={require('../../assets/icons/outlineFavorite.png')}
						/>
					);
				},
			}),
		},
	},
	{
		headerMode: 'screen',
		animationEnabled: false,
		//swipeEnabled: false,
		initialRouteName: 'Home',
		navigationOptions: {
			gestureEnabled: false,
		},
		tabBarOptions: {
			activeTintColor: '#92E087',
			inactiveTintColor: '#fff',
			style: {
				backgroundColor: '#F28421',
				borderTopLeftRadius: 25,
				borderTopRightRadius: 25,
				height: 50,
				paddingVertical: 5,
				shadowOpacity: 0.5,
				shadowColor: '#000',
				shadowOffset: {
					width: 3,
					height: 3,
				},
				elevation: 3,
			},
			// labelStyle: {
			//   fontSize: 12,
			//   lineHeight: 20,
			// },
		},
	},
);

const MyStackNavigator = createStackNavigator(
	{
		Splash: {
			screen: SplashScreen,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						null;
					},
					headerLeft: () => {
						null;
					},
					headerRight: () => {
						null;
					},
				};
			},
		},

		Login: {
			screen: Login,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						null;
					},
					headerLeft: () => {
						null;
					},
					headerRight: () => {
						null;
					},
				};
			},
		},

		Register: {
			screen: Register,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						null;
					},
					headerLeft: () => {
						null;
					},
					headerRight: () => {
						null;
					},
				};
			},
		},

		Checkcode: {
			screen: Checkcode,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						null;
					},
					headerLeft: () => {
						null;
					},
					headerRight: () => {
						null;
					},
				};
			},
		},

		BottomTab: {
			screen: MyBottomTab,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						null;
					},
					headerLeft: () => {
						null;
					},
					headerRight: () => {
						null;
					},
					gestureEnabled: false,
				};
			},
		},
		getCurrentLocation: {
			screen: getCurrentLocation,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						null;
					},
					headerLeft: () => {
						null;
					},
					headerRight: () => {
						null;
					},
				};
			},
		},

		ContactUs: {
			screen: ContactUsScreen,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						return (
							<Header
								navigation={navigation}
								title={i18n.t('contactUs')}
								backButton
							/>
						);
					},
				};
			},
		},
		AddFarm: {
			screen: AddFarmScreen,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						return (
							<Header
								navigation={navigation}
								title={i18n.t('addFarm')}
								backButton
							/>
						);
					},
				};
			},
		},
		MyAdvertisements: {
			screen: MyAdvertisementsScreen,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						return (
							<Header
								navigation={navigation}
								title={i18n.t('myAdvertisements')}
								backButton
							/>
						);
					},
				};
			},
		},
		AddReport: {
			screen: AddReportScreen,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						return (
							<Header
								navigation={navigation}
								title={i18n.t('addReport')}
								backButton
							/>
						);
					},
				};
			},
		},
		AppointmentBooking: {
			screen: AppointmentBookingScreen,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						return (
							<Header
								navigation={navigation}
								title={i18n.t('bookNow')}
								backButton
							/>
						);
					},
				};
			},
		},
		ProviderBookingRequests: {
			screen: ProviderBookingRequestsScreen,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						return (
							<Header
								navigation={navigation}
								title={i18n.t('bookingRequests')}
								backButton
							/>
						);
					},
				};
			},
		},
		ProviderBookingRequestsInfo: {
			screen: ProviderBookingRequestsInfoScreen,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						return (
							<Header
								navigation={navigation}
								title={i18n.t('bookingRequests')}
								backButton
							/>
						);
					},
				};
			},
		},

		// userBookingRequests: {
		//   screen: userBookingRequestsScreen,
		//   navigationOptions: ({ navigation }) => {
		//     return {
		//       header: () => {
		//         return (
		//           <Header
		//             navigation={navigation}
		//             title={i18n.t("myBookings")}
		//             backButton
		//           />
		//         );
		//       },
		//     };
		//   },
		// },
		// UserBookingRequestsInfo: {
		//   screen: UserBookingRequestsInfoScreen,
		//   navigationOptions: ({ navigation }) => {
		//     return {
		//       header: () => {
		//         return (
		//           <Header
		//             navigation={navigation}
		//             title={i18n.t("myBookings")}
		//             backButton
		//           />
		//         );
		//       },
		//     };
		//   },
		// },
		Settings: {
			screen: SettingsScreen,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						return (
							<Header
								navigation={navigation}
								title={i18n.t('settings')}
								backButton
							/>
						);
					},
				};
			},
		},
		SetLanguage: {
			screen: SetLanguageScreen,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						return (
							<Header
								navigation={navigation}
								title={i18n.t('settings')}
								backButton
							/>
						);
					},
				};
			},
		},
		PrivacyPolicy: {
			screen: PrivacyPolicy,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						return (
							<Header
								navigation={navigation}
								title={'Privacy Policy'}
								backButton
							/>
						);
					},
				};
			},
		},
		ProfileScreen: {
			screen: ProfileScreen,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						return (
							<Header
								navigation={navigation}
								title={'Profile Screen'}
								backButton
							/>
						);
					},
				};
			},
		},
		ChangePassword: {
			screen: ChangePassword,
			navigationOptions: ({ navigation }) => {
				return {
					header: () => {
						return (
							<Header
								navigation={navigation}
								title={'Change Password'}
								backButton
							/>
						);
					},
				};
			},
		},
	},

	{
		//swipeEnabled: false,

		headerMode: 'screen',
		initialRouteName: 'Splash',
	},
);

// tabBarComponent: props =>{
// return(
// <React.Fragment>
// <TabBarComponent {...props} />
// <Image style={{width: "100%", height: 50}} source = {require('../components/assets/image1/footerend.png')}/>
// </React.Fragment>
// )
// }

const MainDrawer = createDrawerNavigator(
	{
		TabStack: {
			screen: MyStackNavigator,
			navigationOptions: ({ navigation }) => {
				return {
					drawerLockMode:
						navigation.state.routes[navigation.state.index].routeName ===
							'Splash' ||
						navigation.state.routes[navigation.state.index].routeName ===
							'Login' ||
						navigation.state.routes[navigation.state.index].routeName ===
							'Register'
							? 'locked-closed'
							: 'unlocked',

					// disableGestures:
					// navigation.state.routes[navigation.state.index].routeName ===
					//     "Splash" ||
					//   navigation.state.routes[navigation.state.index].routeName ===
					//     "Login" ||
					//   navigation.state.routes[navigation.state.index].routeName ===
					//     "Register" ||
					//   navigation.state.routes[navigation.state.index].routeName ===
					//     "RegisterLogin"
					//     ? false
					//     : false,
				};
			},
		},
	},

	{
		drawerType: 'back',
		drawerPosition: I18nManager.isRTL ? 'right' : 'left',
		initialRouteName: 'TabStack',
		contentComponent: DrawerComponent,
	},
);

export default MainDrawer;

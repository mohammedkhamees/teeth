import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Alert,
} from "react-native";
import { AntDesign } from "@expo/vector-icons";
import styles from "./style";
import axios from "axios";

const CheckCode = ({ navigation }) => {
  const [code, setCode] = useState("");

  const email = navigation.getParam("email");

  const getCode = (token) => {
    if (email) {
      axios
        .post("https://nextstageksa.com/clinic/api/validatecode", { code })
        .then((res) => {
          navigation.navigate("NewPass", {
            email,
          });
        })
        .catch((error) => {
          Alert.alert("Please enter a valid code");
        });
    } else {
      Alert.alert(
        "Forget Password",
        `Please enter the code that have been sent to ${email}`
      );
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.textContainer}>
        <TouchableOpacity
          style={styles.iconContainer}
          onPress={() => navigation.goBack()}
        >
          <AntDesign name="left" style={styles.icon} />
        </TouchableOpacity>
        <Text style={styles.text}>Forgot Password</Text>
      </View>
      <View style={styles.container}>
        <Text style={styles.textDesc}>Please enter the activation code</Text>
        <Text style={styles.textDesc}>that have been sent to your email</Text>

        <TextInput
          style={styles.inputBox}
          autoCorrect={false}
          placeholder={"Confirmation Code (5 - digit)"}
          placeholderTextColor="#768AE1"
          selectionColor="#004FFE"
          keyboardType="number-pad"
          enablesReturnKeyAutomatically={true}
          onChangeText={setCode}
        />

        <TouchableOpacity onPress={getCode} style={styles.btn}>
          <Text style={styles.btnText}>Change Password</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CheckCode;

CheckCode.navigationOptions = () => {
  return {
    header: () => false,
  };
};

import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Alert,
} from "react-native";
import { AntDesign } from "@expo/vector-icons";
import styles from "./style";
import axios from "axios";

const ForgetPassword = ({ navigation }) => {
  const [email, setEmail] = useState("");

  const resetPass = (token) => {
    if (email) {
      axios
        .post("https://nextstageksa.com/clinic/api/sendpassword", { email })
        .then((res) => {
          navigation.navigate("CheckCode", {
            email,
          });
        })
        .catch((error) => {
          Alert.alert("Please enter a valid email");
        });
    } else {
      Alert.alert("Forget Password", "Please enter your email");
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.textContainer}>
        <TouchableOpacity
          style={styles.iconContainer}
          onPress={() => navigation.goBack()}
        >
          <AntDesign name="left" style={styles.icon} />
        </TouchableOpacity>
        <Text style={styles.text}>Forgot Password</Text>
      </View>
      <View style={styles.container}>
        <TextInput
          style={styles.inputBox}
          autoCorrect={false}
          autoCapitalize={"none"}
          placeholder={"Enter Your Email Here..."}
          placeholderTextColor="#768AE1"
          selectionColor="#004FFE"
          keyboardType="email-address"
          enablesReturnKeyAutomatically={true}
          onChangeText={setEmail}
        />
        <TouchableOpacity onPress={resetPass} style={styles.btn}>
          <Text style={styles.btnText}>Reset Password</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ForgetPassword;

ForgetPassword.navigationOptions = () => {
  return {
    header: () => false,
  };
};

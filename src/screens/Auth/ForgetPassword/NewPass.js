import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Alert,
} from "react-native";
import { AntDesign } from "@expo/vector-icons";
import styles from "./style";
import axios from "axios";

const NewPass = ({ navigation }) => {
  const [password, setPassword] = useState("");
  const [configPass, setConfigPass] = useState("");

  const email = navigation.getParam("email");

  const changePass = (token) => {
    if (password && configPass) {
      if (password === configPass) {
        axios
          .post("https://nextstageksa.com/clinic/api/changepassword", {
            password,
            email,
          })
          .then((res) => {
            Alert.alert(
              "Reset",
              "Password has been changed successfully",
              [{ text: "OK", onPress: () => navigation.navigate("Login") }],
              { cancelable: true }
            );
          })
          .catch((error) => {
            Alert.alert("Reset", "Please check the Passwords :) ");
          });
      }
    } else {
      Alert.alert("Reset", "Please Fill all the fields");
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.textContainer}>
        <TouchableOpacity
          style={styles.iconContainer}
          onPress={() => navigation.goBack()}
        >
          <AntDesign name="left" style={styles.icon} />
        </TouchableOpacity>
        <Text style={styles.text}>Forgot Password</Text>
      </View>
      <View style={styles.container}>
        <TextInput
          style={styles.inputBox}
          autoCorrect={false}
          placeholder={"New Password..."}
          autoCapitalize={"none"}
          placeholderTextColor="#768AE1"
          selectionColor="#004FFE"
          keyboardType="email-address"
          enablesReturnKeyAutomatically={true}
          onChangeText={setPassword}
          secureTextEntry={true}
        />

        <TextInput
          style={styles.inputBox}
          autoCorrect={false}
          placeholder={"Confirm Password..."}
          autoCapitalize={"none"}
          placeholderTextColor="#768AE1"
          selectionColor="#004FFE"
          keyboardType="email-address"
          enablesReturnKeyAutomatically={true}
          onChangeText={setConfigPass}
          secureTextEntry={true}
        />

        <TouchableOpacity onPress={changePass} style={styles.btn}>
          <Text style={styles.btnText}>Change Password</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default NewPass;

NewPass.navigationOptions = () => {
  return {
    header: () => false,
  };
};

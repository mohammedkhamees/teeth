import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: { flex: 1, alignItems: "center", justifyContent: "center" },

  inputBox: {
    width: 250,
    height: 50,
    borderBottomColor: "#6B87D6",
    borderBottomWidth: 1,
    color: "#004FFE",
    fontSize: 15,
    borderColor: "red",
    marginVertical: 5,
  },

  textContainer: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#004FFE",
    height: "12%",
    borderBottomEndRadius: 35,
    borderBottomLeftRadius: 35,
    position: "absolute",
    top: 0,
    width: "100%",
  },

  text: { color: "#fff", fontSize: 22, marginTop: "5%" },

  textDesc: {
    fontSize: 22,
    color: "#3655DD",
    textAlign: "center",
  },

  btn: {
    width: "70%",
    alignItems: "center",
    backgroundColor: "#004FFE",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderWidth: 2,
    borderRadius: 28,
    borderColor: "white",
    marginTop: 30,
  },

  btnText: {
    color: "#fff",
    fontSize: 25,
    fontWeight: "bold",
  },

  icon: {
    fontSize: 25,
    color: "white",
  },

  iconContainer: {
    position: "absolute",
    top: "50%",
    left: "5%",
  },
});

export default styles;

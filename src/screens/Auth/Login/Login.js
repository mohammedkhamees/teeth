import React, { useContext, useState } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Alert,
} from "react-native";
import logo from "./../../../../assets/images/logo.png";
import { Context as AuthContext } from "./../../../context/AuthContext";
import styles from "./style";

const Login = ({ navigation }) => {
  const { signin } = useContext(AuthContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const key = "6Q4AE52F15F8B82735E9485CA3EC7";

  const goSignIn = () => {
    if (email && password) {
      signin({ email, password, key });
    } else {
      Alert.alert("Please Fill all the fields");
    }
  };

  const forgetPassword = () => {
    navigation.navigate("ForgetPassword");
  };

  return (
    <View style={styles.container}>
      <View style={styles.imgContainer}>
        <Image source={logo} style={styles.img} />
      </View>

      <View style={styles.formContainer}>
        <TextInput
          style={styles.textInput}
          autoCapitalize={"none"}
          placeholder="Your Email..."
          keyboardType="email-address"
          onChangeText
          onChangeText={setEmail}
        />
        <TextInput
          style={styles.textInput}
          autoCapitalize={"none"}
          placeholder="Your Password..."
          onChangeText={setPassword}
          secureTextEntry={true}
        />
        <TouchableOpacity onPress={goSignIn} style={styles.btn}>
          <Text style={styles.btnText}>Login</Text>
        </TouchableOpacity>

        <Text onPress={forgetPassword} style={styles.forgetPassword}>
          Forgot Password
        </Text>
      </View>
    </View>
  );
};

export default Login;

Login.navigationOptions = () => {
  return {
    header: () => false,
  };
};

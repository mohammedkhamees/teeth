import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#305FDF",
    justifyContent: "center",
    alignItems: "center",
  },

  imgContainer: {
    justifyContent: "center",
    alignSelf: "center",
    marginTop: 15,
    height: "40%",
  },

  img: { width: 300, height: 140 },

  formContainer: {
    width: "100%",
    height: "60%",
    backgroundColor: "#fff",
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-start",
    paddingTop: "20%",
  },

  forgetPassword: {
    borderBottomWidth: 2,
    borderBottomColor: "#888B93",
    fontSize: 17,
    color: "#888B93",
    marginTop: 20,
  },

  textInput: {
    width: 200,
    height: 30,
    margin: 12,
    borderBottomWidth: 1,
    borderColor: "#B0B2B5",
  },

  btn: {
    width: "50%",
    alignItems: "center",
    backgroundColor: "#004FFE",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderWidth: 2,
    borderRadius: 28,
    borderColor: "white",
    marginTop: 30,
  },

  btnText: {
    color: "#fff",
    fontSize: 25,
    fontWeight: "bold",
  },
});

export default styles;

import React, { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  ActivityIndicator,
  Alert,
} from "react-native";
import profilepc from "./../../../../../assets/images/profilepc.png";
import changeProPc from "./../../../../../assets/images/profileImage.png";
import * as ImagePicker from "expo-image-picker";
import styles from "./style";

const FirstRegister = ({ navigation }) => {
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [configPass, setConfigPass] = useState("");
  const [profile_image, setImage] = useState("");

  const submitRegister = () => {
    if (name && phone && email && password && configPass && profile_image) {
      if (password === configPass) {
        navigation.navigate("SecondRegister", {
          name,
          email,
          phone,
          password,
          configPass,
          profile_image,
        });
      } else {
        Alert.alert("Please check Password :) ");
      }
    } else {
      Alert.alert("Please fill all the fields");
    }
  };

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [16, 9],
      quality: 1,
      base64: true,
    });

    // const base64Icon = `data:image/png;base64,${result.base64}`;

    if (!result.cancelled) {
      setImage({
        ext: "png",
        base64: `${result.base64}`,
      });
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.profilePictureWrapper}>
        {profile_image != "" ? (
          <Image
            source={{ uri: `data:image/png;base64,${profile_image.base64}` }}
            style={styles.profilePicture}
            resizeMode="contain"
            PlaceholderContent={<ActivityIndicator />}
          />
        ) : (
          <Image
            source={profilepc}
            style={styles.profilePicture}
            resizeMode="contain"
            PlaceholderContent={<ActivityIndicator />}
          />
        )}
        <TouchableOpacity style={styles.upload} onPress={pickImage}>
          <Text style={styles.text}>Upload</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.formContainer}>
        <TextInput
          style={styles.inputBox}
          autoCorrect={false}
          placeholder={"Name"}
          autoCapitalize={"none"}
          placeholderTextColor="#305FDF"
          selectionColor="#305FDF"
          enablesReturnKeyAutomatically={true}
          onChangeText={setName}
        />
        <TextInput
          style={styles.inputBox}
          autoCorrect={false}
          placeholder={"Phone Number"}
          placeholderTextColor="#305FDF"
          selectionColor="#305FDF"
          keyboardType="phone-pad"
          enablesReturnKeyAutomatically={true}
          onChangeText={setPhone}
        />
        <TextInput
          style={styles.inputBox}
          autoCorrect={false}
          placeholder={"Email"}
          autoCapitalize={"none"}
          placeholderTextColor="#305FDF"
          selectionColor="#305FDF"
          keyboardType="email-address"
          enablesReturnKeyAutomatically={true}
          onChangeText={setEmail}
        />
        <TextInput
          style={styles.inputBox}
          autoCorrect={false}
          placeholder={"Password"}
          autoCapitalize={"none"}
          placeholderTextColor="#305FDF"
          selectionColor="#305FDF"
          enablesReturnKeyAutomatically={true}
          onChangeText={setPassword}
          secureTextEntry={true}
        />
        <TextInput
          style={styles.inputBox}
          autoCorrect={false}
          autoCapitalize={"none"}
          placeholder={"Confirm Password"}
          placeholderTextColor="#305FDF"
          selectionColor="#305FDF"
          enablesReturnKeyAutomatically={true}
          onChangeText={setConfigPass}
          secureTextEntry={true}
        />
        <TouchableOpacity onPress={submitRegister} style={styles.btn}>
          <Text style={styles.btnText}>Next</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default FirstRegister;

FirstRegister.navigationOptions = () => {
  return {
    header: () => false,
  };
};

import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    height: "100%",
    paddingTop: "60%",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  inputBox: {
    width: 250,
    height: 50,
    borderBottomColor: "#6B87D6",
    borderBottomWidth: 1,
    color: "blue",
    fontSize: 15,
    borderColor: "red",
  },
  profilePictureWrapper: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 50,
  },
  profilePicture: {
    width: height / 3.7,
    height: height / 3.7,
    resizeMode: "cover",
    borderRadius: height / 6,
  },

  upload: {
    marginTop: -40,
  },

  text: {
    fontSize: 13,
    color: "#fff",
    marginBottom: 10,
  },

  btn: {
    width: "70%",
    alignItems: "center",
    backgroundColor: "#305FDF",
    padding: 10,
    borderWidth: 2,
    borderRadius: 28,
    borderColor: "white",
    marginTop: 20,
  },

  btnText: { color: "#fff", fontSize: 25, fontWeight: "bold" },

  formContainer: {
    backgroundColor: "#fff",
    width: "100%",
    height: "100%",
    alignItems: "center",
  },
});

export default styles;

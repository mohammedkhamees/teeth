import React, { useState, useContext, useEffect } from "react";
import {
  View,
  Text,
  Modal,
  Alert,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Image,
} from "react-native";
import Logo from "./../../../../../assets/images/bluelogo.png";
import styles from "./style";

const Review = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View>
        <Image source={Logo} />
      </View>
      <View style={styles.textContainer}>
        <Text style={styles.text}>Thank You For Signing up at</Text>
        <Text style={styles.text}>Teeth App</Text>
        <Text style={styles.text}>We will check your form and get</Text>
        <Text style={styles.text}>back to you soon</Text>
      </View>
      <TouchableOpacity
        onPress={() => navigation.navigate("Welcome")}
        style={styles.btn}
      >
        <Text style={styles.btnText}>Got it</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Review;

Review.navigationOptions = () => {
  return {
    header: () => false,
  };
};

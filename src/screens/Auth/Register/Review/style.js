import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 150,
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#fff",
  },
  textContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 200,
  },
  text: {
    fontSize: 18,
    color: "#004FFE",
  },
  btn: {
    width: "60%",
    alignItems: "center",
    backgroundColor: "#305FDF",
    padding: 10,
    borderWidth: 2,
    borderRadius: 28,
    borderColor: "white",
    marginBottom: 100,
  },
  btnText: { color: "#fff", fontSize: 20 },
});

export default styles;

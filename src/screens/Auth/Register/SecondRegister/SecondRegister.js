import React, { useState, useEffect, useContext } from "react";
import {
  View,
  Text,
  Modal,
  FlatList,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Image,
  ActivityIndicator,
  Alert,
} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { RadioButton } from "react-native-paper";
import profilepc from "./../../../../../assets/images/profilepc.png";
import { Context as AuthContext } from "./../../../../context/AuthContext";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import * as OpenAnything from "react-native-openanything";
import styles from "./style";

const SecondRegister = ({ navigation }) => {
  const [token, setToken] = useState("");
  const [id_city, setCityID] = useState("");
  const [cityName, setCityName] = useState("");
  const [Gender, setGender] = useState("");
  const [agree, setAgree] = useState(false);
  const [key_District, setCheckedaddress] = useState("");
  const [address, setAddress] = useState("");
  const [role_id, setRole_id] = useState(0);
  const [Age, setAge] = useState(0);
  const [location, setLocation] = useState([]);
  const [signingUpAs, setSigningUpAs] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const [modalVisibleGender, setModalVisibleGender] = useState(false);
  const [modalVisibleaddress, setModalVisibleaddress] = useState(false);
  const key = "6Q4AE52F15F8B82735E9485CA3EC7";
  const lat = "150.644";
  const lng = "34.397";

  const { signup } = useContext(AuthContext);
  const name = navigation.getParam("name");
  const phone = navigation.getParam("phone");
  const email = navigation.getParam("email");
  const password = navigation.getParam("password");
  const configPass = navigation.getParam("configPass");
  const profile_image = navigation.getParam("profile_image");

  const Producto = ({ equipo, nombre }) => {
    return (
      <View style={{ flexDirection: "row", alignItems: "center", padding: 5 }}>
        <View
          style={{
            borderWidth: Platform.OS === "ios" ? 1 : 0,
            borderRadius: 22,
            borderColor: "#969698",
          }}
        >
          <RadioButton
            value={nombre}
            status={key_District === nombre ? "checked" : "unchecked"}
            onPress={() => {
              setCheckedaddress(nombre);
              setAddress(equipo);
            }}
            color="#969698"
          />
        </View>
        <View>
          <Text style={{ color: "#969698" }}> {equipo} </Text>
        </View>
      </View>
    );
  };

  const selectCity = () => {
    setLocation("");
    setModalVisible(true);
  };

  const getDoctor = () => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/FindDistrict?key=6Q4AE52F15F8B82735E9485CA3EC7&id_city=${id_city}`,
        { token }
      )
      .then((response) => {
        setLocation(response.data.Data.Loctions);
      })
      .catch((error) => {
        console.log("ERROR CITY", error);
      });
  };

  const goSignUp = () => {
    if (cityName && Gender && agree && role_id && Age && key_District) {
      if (role_id === 4) {
        signup({
          name,
          phone,
          email,
          password,
          configPass,
          profile_image,
          role_id,
          Gender,
          Age,
          lng,
          lat,
          key,
          id_city,
          key_District,
        });
      } else {
        navigation.navigate("ThirdRegister", {
          name,
          phone,
          email,
          password,
          configPass,
          profile_image,
          role_id,
          Gender,
          Age,
          lng,
          lat,
          key,
          id_city,
          key_District,
        });
      }
    } else {
      Alert.alert("Please Fill all the fields");
    }
  };

  const getlocation = () => {
    setModalVisible(false);
    getDoctor();
  };

  return (
    <ScrollView>
      <View style={{ height: 33 }} />
      <View style={{ flex: 1 }}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            setModalVisible(!modalVisible);
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <TouchableOpacity onPress={() => setModalVisible(false)}>
                <Text style={styles.modalText}>City</Text>
              </TouchableOpacity>

              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 0.5, padding: 20 }}>
                  <View style={styles.radiobtnContainer}>
                    <View style={styles.radiobtn}>
                      <RadioButton
                        value="0"
                        status={id_city === "0" ? "checked" : "unchecked"}
                        onPress={() => {
                          setCityID("0");
                          setCityName("Amman");
                          setAddress("");
                          setCheckedaddress("");
                        }}
                        color="#969698"
                      />
                    </View>
                    <View>
                      <Text style={{ color: "#969698" }}> Amman </Text>
                    </View>
                  </View>

                  <View style={styles.radiobtnContainer}>
                    <View style={styles.radiobtn}>
                      <RadioButton
                        value="2"
                        status={id_city === "2" ? "checked" : "unchecked"}
                        onPress={() => {
                          setCityID("2");
                          setCityName("Irbid");
                          setAddress("");
                          setCheckedaddress("");
                        }}
                        color="#969698"
                      />
                    </View>
                    <View>
                      <Text style={{ color: "#969698" }}> Irbid </Text>
                    </View>
                  </View>

                  <View style={styles.radiobtnContainer}>
                    <View style={styles.radiobtn}>
                      <RadioButton
                        value="1"
                        status={id_city === "1" ? "checked" : "unchecked"}
                        onPress={() => {
                          setCityID("1");
                          setCityName("Zarqa");
                          setAddress("");
                          setCheckedaddress("");
                        }}
                        color="#969698"
                      />
                    </View>
                    <View>
                      <Text style={{ color: "#969698" }}> Zarqa </Text>
                    </View>
                  </View>

                  <View style={styles.radiobtnContainer}>
                    <View style={styles.radiobtn}>
                      <RadioButton
                        value="3"
                        status={id_city === "3" ? "checked" : "unchecked"}
                        onPress={() => {
                          setCityID("3");
                          setCityName("Aqaba");
                          setAddress("");
                          setCheckedaddress("");
                        }}
                        color="#969698"
                      />
                    </View>
                    <View>
                      <Text style={{ color: "#969698" }}> Aqaba </Text>
                    </View>
                  </View>

                  <View style={styles.radiobtnContainer}>
                    <View style={styles.radiobtn}>
                      <RadioButton
                        value="7"
                        status={id_city === "7" ? "checked" : "unchecked"}
                        onPress={() => {
                          setCityID("7");
                          setCityName("Jarash");
                          setAddress("");
                          setCheckedaddress("");
                        }}
                        color="#969698"
                      />
                    </View>
                    <View>
                      <Text style={{ color: "#969698" }}> Jarash </Text>
                    </View>
                  </View>

                  <View style={styles.radiobtnContainer}>
                    <View style={styles.radiobtn}>
                      <RadioButton
                        value="4"
                        status={id_city === "4" ? "checked" : "unchecked"}
                        onPress={() => {
                          setCityID("4");
                          setCityName("Mafraq");
                          setAddress("");
                          setCheckedaddress("");
                        }}
                        color="#969698"
                      />
                    </View>
                    <View>
                      <Text style={{ color: "#969698" }}> Mafraq </Text>
                    </View>
                  </View>

                  <View style={styles.radiobtnContainer}>
                    <View style={styles.radiobtn}>
                      <RadioButton
                        value="5"
                        status={id_city === "5" ? "checked" : "unchecked"}
                        onPress={() => {
                          setCityID("5");
                          setCityName("Madaba");
                          setAddress("");
                          setCheckedaddress("");
                        }}
                        color="#969698"
                      />
                    </View>
                    <View>
                      <Text style={{ color: "#969698" }}> Madaba </Text>
                    </View>
                  </View>

                  <View style={styles.radiobtnContainer}>
                    <View style={styles.radiobtn}>
                      <RadioButton
                        value="6"
                        status={id_city === "6" ? "checked" : "unchecked"}
                        onPress={() => {
                          setCityID("6");
                          setCityName("Al-Balqa");
                          setAddress("");
                          setCheckedaddress("");
                        }}
                        color="#969698"
                      />
                    </View>
                    <View>
                      <Text style={{ color: "#969698" }}> Al-Balqa </Text>
                    </View>
                  </View>

                  <View style={styles.radiobtnContainer}>
                    <View style={styles.radiobtn}>
                      <RadioButton
                        value="8"
                        status={id_city === "8" ? "checked" : "unchecked"}
                        onPress={() => {
                          setCityID("8");
                          setCityName("Ma'an");
                          setAddress("");
                          setCheckedaddress("");
                        }}
                        color="#969698"
                      />
                    </View>
                    <View>
                      <Text style={{ color: "#969698" }}> Ma'an </Text>
                    </View>
                  </View>

                  <View style={styles.radiobtnContainer}>
                    <View style={styles.radiobtn}>
                      <RadioButton
                        value="9"
                        status={id_city === "9" ? "checked" : "unchecked"}
                        onPress={() => {
                          setCityID("9");
                          setCityName("Karak");
                          setAddress("");
                          setCheckedaddress("");
                        }}
                        color="#969698"
                      />
                    </View>
                    <View>
                      <Text style={{ color: "#969698" }}> Karak </Text>
                    </View>
                  </View>
                  <View style={styles.radiobtnContainer}>
                    <View style={styles.radiobtn}>
                      <RadioButton
                        value="11"
                        status={id_city === "11" ? "checked" : "unchecked"}
                        onPress={() => {
                          setCityID("11");
                          setCityName("Ajloun");
                          setAddress("");
                          setCheckedaddress("");
                        }}
                        color="#969698"
                      />
                    </View>
                    <View>
                      <Text style={{ color: "#969698" }}> Ajloun </Text>
                    </View>
                  </View>

                  <View style={styles.radiobtnContainer}>
                    <View style={styles.radiobtn}>
                      <RadioButton
                        value="10"
                        status={id_city === "10" ? "checked" : "unchecked"}
                        onPress={() => {
                          setCityID("10");
                          setCityName("Tafilah");
                          setAddress("");
                          setCheckedaddress("");
                        }}
                        color="#969698"
                      />
                    </View>
                    <View>
                      <Text style={{ color: "#969698" }}> Tafilah </Text>
                    </View>
                  </View>
                </View>
              </View>

              <TouchableOpacity
                onPress={() => getlocation()}
                style={styles.doneBtn}
              >
                <Text style={styles.doneBtnText}>Done</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisibleGender}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            setModalVisibleGender(!modalVisibleGender);
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <TouchableOpacity onPress={() => setModalVisibleGender(false)}>
                <Text style={styles.modalText}> Gender </Text>
              </TouchableOpacity>

              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 0.5, padding: 20 }}>
                  <View style={styles.radiobtnContainer}>
                    <View style={styles.radiobtn}>
                      <RadioButton
                        value="Male"
                        status={Gender === "Male" ? "checked" : "unchecked"}
                        onPress={() => setGender("Male")}
                        color="#969698"
                      />
                    </View>
                    <View>
                      <Text style={{ color: "#969698" }}> Male </Text>
                    </View>
                  </View>

                  <View style={styles.radiobtnContainer}>
                    <View style={styles.radiobtn}>
                      <RadioButton
                        value="Female"
                        status={Gender === "Female" ? "checked" : "unchecked"}
                        onPress={() => setGender("Female")}
                        color="#969698"
                      />
                    </View>
                    <View>
                      <Text style={{ color: "#969698" }}> Female </Text>
                    </View>
                  </View>
                </View>
              </View>

              <TouchableOpacity
                onPress={() => setModalVisibleGender(false)}
                style={styles.doneBtn}
              >
                <Text style={styles.doneBtnText}>Done</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisibleaddress}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            setModalVisibleaddress(!modalVisibleaddress);
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <TouchableOpacity onPress={() => setModalVisibleaddress(false)}>
                <Text style={styles.modalText}> Address </Text>
              </TouchableOpacity>

              <View style={{ flexDirection: "row" }}>
                <View style={{ padding: 20 }}>
                  <FlatList
                    data={location}
                    renderItem={({ item }) => (
                      <Producto equipo={item.name} nombre={item.key_id} />
                    )}
                    keyExtractor={(item) => item.key_id}
                  />
                </View>
              </View>

              <TouchableOpacity
                onPress={() => setModalVisibleaddress(false)}
                style={styles.doneBtn}
              >
                <Text style={styles.doneBtnText}>Done</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            marginTop: "35%",
          }}
        >
          <View style={styles.profilePictureWrapper}>
            <Image
              source={{ uri: `data:image/png;base64,${profile_image.base64}` }}
              style={styles.profilePicture}
              resizeMode="contain"
              PlaceholderContent={<ActivityIndicator />}
            />
          </View>

          <View
            style={{
              backgroundColor: "#fff",
              width: "100%",
              height: "100%",
              alignItems: "center",
              paddingTop: "5%",
            }}
          >
            <View>
              <TouchableOpacity
                style={styles.inputBox}
                onPress={() => setModalVisibleGender(true)}
              >
                <Text style={{ color: "#305FDF" }}>
                  {Gender ? Gender : "Gender"}
                </Text>
              </TouchableOpacity>
              <TextInput
                style={styles.inputBox}
                autoCorrect={false}
                placeholder={"Age"}
                placeholderTextColor="#305FDF"
                selectionColor="#305FDF"
                keyboardType="numeric"
                enablesReturnKeyAutomatically={true}
                onChangeText={setAge}
              />
              <TouchableOpacity
                style={styles.inputBox}
                onPress={() => selectCity()}
              >
                <Text style={{ color: "#305FDF" }}>
                  {cityName ? cityName : "City"}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.inputBox}
                onPress={() => setModalVisibleaddress(true)}
              >
                <Text style={{ color: "#305FDF" }}>
                  {address ? address : "Address"}
                </Text>
              </TouchableOpacity>
              {signingUpAs == true ? (
                <View
                  style={{
                    flexDirection: "row",
                    borderBottomWidth: 1,
                    height: 50,
                    borderBottomColor: "#305FDF",
                    justifyContent: "center",
                    alignSelf: "center",
                  }}
                >
                  <Text
                    style={{
                      color: "#305FDF",
                      marginLeft: 110,
                      alignSelf: "center",
                    }}
                  >
                    Im signingUp as a{"  "}
                  </Text>

                  <TouchableOpacity
                    style={{ alignSelf: "center" }}
                    onPress={() => setSigningUpAs(false)}
                  >
                    <FontAwesome5
                      active
                      size={20}
                      name={"arrow-down"}
                      color="#305FDF"
                    />
                  </TouchableOpacity>
                </View>
              ) : (
                <View
                  style={{ borderBottomWidth: 1, borderBottomColor: "#305FDF" }}
                >
                  <View style={{ flexDirection: "row", height: 50 }}>
                    <Text style={{ color: "#305FDF", alignSelf: "center" }}>
                      Im signingUp as a{"   "}
                    </Text>
                    <TouchableOpacity
                      style={{ alignSelf: "center" }}
                      onPress={() => setSigningUpAs(true)}
                    >
                      <FontAwesome5
                        active
                        size={20}
                        name={"arrow-left"}
                        color="#305FDF"
                      />
                    </TouchableOpacity>
                  </View>
                  <View style={{}}>
                    <View style={styles.radiobtnContainer}>
                      <View style={styles.radiobtn}>
                        <RadioButton
                          value="3"
                          status={role_id === 3 ? "checked" : "unchecked"}
                          onPress={() => {
                            setRole_id(3);
                            setSigningUpAs(true);
                          }}
                          color="#305FDF"
                        />
                      </View>
                      <Text style={{ color: "#305FDF", fontSize: 13 }}>
                        {"  "}
                        Drugstor
                      </Text>
                    </View>

                    <View style={styles.radiobtnContainer}>
                      <View style={styles.radiobtn}>
                        <RadioButton
                          value="2"
                          status={role_id === 2 ? "checked" : "unchecked"}
                          onPress={() => {
                            setRole_id(2);
                            setSigningUpAs(true);
                          }}
                          color="#305FDF"
                        />
                      </View>
                      <Text style={{ color: "#305FDF", fontSize: 13 }}>
                        {"  "}
                        Doctor
                      </Text>
                    </View>

                    <View style={styles.radiobtnContainer}>
                      <View style={styles.radiobtn}>
                        <RadioButton
                          value="4"
                          status={role_id === 4 ? "checked" : "unchecked"}
                          onPress={() => setRole_id(4)}
                          color="#305FDF"
                        />
                      </View>
                      <Text style={{ color: "#305FDF", fontSize: 13 }}>
                        {"  "}
                        User
                      </Text>
                    </View>
                  </View>
                </View>
              )}
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  marginTop: 10,
                }}
              >
                <View
                  style={{
                    borderWidth: Platform.OS === "ios" ? 1 : 0,
                    borderColor: "#969698",
                  }}
                >
                  <RadioButton
                    value="true"
                    status={agree === true ? "checked" : "unchecked"}
                    onPress={() => setAgree(!agree)}
                    color="#305FDF"
                  />
                </View>
                <TouchableOpacity
                  onPress={() =>
                    OpenAnything.Pdf("https://www.teeth-jo.com/pdf/policy.pdf")
                  }
                >
                  <Text style={{ color: "#305FDF", fontSize: 13 }}>
                    {"  "}agree on the terms and conditions
                  </Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={{ height: 32 }} />
            <TouchableOpacity
              onPress={() => goSignUp()}
              style={{
                width: "70%",
                alignItems: "center",
                backgroundColor: "#305FDF",
                padding: 10,
                borderWidth: 2,
                borderRadius: 28,
                borderColor: "white",
              }}
            >
              <Text style={{ color: "#fff", fontSize: 25, fontWeight: "bold" }}>
                Create Account
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default SecondRegister;

SecondRegister.navigationOptions = () => {
  return {
    header: () => false,
  };
};

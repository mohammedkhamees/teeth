import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  inputBox: {
    width: 250,
    height: 50,
    borderBottomColor: "#6B87D6",
    borderBottomWidth: 1,
    color: "blue",
    fontSize: 15,
    borderColor: "red",
    justifyContent: "center",
  },
  profilePictureWrapper: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 30,
    marginTop: 40,
  },
  profilePicture: {
    width: height / 3.7,
    height: height / 3.7,
    resizeMode: "cover",
    borderRadius: height / 6,
  },
  imagesGroup: {
    flexWrap: "wrap",
    margin: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    fontSize: 13,
    color: "#fff",
    marginBottom: 10,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    width: "70%",
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  mainContainer: {
    flex: 1,
  },

  flatlistContainer: {
    flex: 10 / 9,
  },

  radiobtn: {
    borderWidth: Platform.OS === "ios" ? 1 : 0,
    borderRadius: 22,
    borderColor: "#969698",
  },

  radiobtnContainer: {
    flexDirection: "row",
    alignItems: "center",
    padding: 5,
  },

  doneBtn: {
    borderRadius: 22,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#305FDF",
    padding: 10,
  },
  doneBtnText: { color: "#fff", fontSize: 20, fontWeight: "bold" },
});

export default styles;

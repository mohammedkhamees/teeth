import React, { useState, useContext, useEffect } from "react";
import {
  View,
  Text,
  Modal,
  Alert,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from "react-native";
import { Context as AuthContext } from "./../../../../context/AuthContext";
import { AntDesign } from "@expo/vector-icons";
import * as OpenAnything from "react-native-openanything";
import { RadioButton } from "react-native-paper";
import * as DocumentPicker from "expo-document-picker";
import * as ImagePicker from "expo-image-picker";
import * as FileSystem from "expo-file-system";
import styles from "./style";

const ThirdRegister = ({ navigation }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [brand, setBrand] = useState("");
  const [specialized, setSpecialized] = useState("");
  const [resident, setResident] = useState("");
  const [agree, setAgree] = useState(false);
  const [image, setCertificate] = useState(null);
  const [national_id, setNationalID] = useState(null);
  const [comm_rec, setCommercialRecord] = useState(null);
  const [certificateAgree, setCertificateAgree] = useState(false);
  const [nationalIDAgree, setNationalIDAgree] = useState(false);
  const [commercialRecordAgree, setCommercialRecordAgree] = useState(false);

  const { signup } = useContext(AuthContext);

  const name = navigation.getParam("name");
  const phone = navigation.getParam("phone");
  const email = navigation.getParam("email");
  const password = navigation.getParam("password");
  const configPass = navigation.getParam("configPass");
  const profile_image = navigation.getParam("profile_image");
  const role_id = navigation.getParam("role_id");
  const Gender = navigation.getParam("Gender");
  const Age = navigation.getParam("Age");
  const lng = navigation.getParam("lng");
  const lat = navigation.getParam("lat");
  const key = navigation.getParam("key");
  const id_city = navigation.getParam("id_city");
  const key_District = navigation.getParam("key_District");

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const { status } =
          await ImagePicker.requestMediaLibraryPermissionsAsync();

        await ImagePicker.requestCameraPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  const goSignUp = () => {
    if (
      agree &&
      (brand || (specialized && resident)) &&
      (commercialRecordAgree || (certificateAgree && setNationalIDAgree))
    ) {
      signup({
        name,
        phone,
        email,
        password,
        configPass,
        profile_image,
        role_id,
        Gender,
        Age,
        lng,
        lat,
        key,
        id_city,
        key_District,
        brand,
        specialized,
        resident,
        comm_rec,
        image,
        national_id,
      });
    } else {
      Alert.alert("Please Fill all the fields");
    }
  };

  const documentSelect = async () => {
    try {
      const file = await DocumentPicker.getDocumentAsync({
        type: "application/pdf",
        copyToCacheDirectory: true,
        multiple: true,
      });

      const fileBase64 = await FileSystem.readAsStringAsync(file.uri, {
        encoding: "base64",
      });

      if (certificateAgree)
        setCertificate({ ext: "pdf", base64: `${fileBase64}` });
      if (nationalIDAgree)
        setNationalID({ ext: "pdf", base64: `${fileBase64}` });
      if (commercialRecordAgree)
        setCommercialRecord({ ext: "pdf", base64: `${fileBase64}` });
    } catch (err) {
      console.log("err", err);
    }
  };

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [16, 9],
      quality: 1,
      base64: true,
    });
    const base64Icon = `data:image/png;base64,${result.base64}`;

    if (!result.cancelled) {
      if (certificateAgree)
        setCertificate({ ext: "png", base64: `${result.base64}` });
      if (nationalIDAgree)
        setNationalID({ ext: "png", base64: `${result.base64}` });
      if (commercialRecordAgree)
        setCommercialRecord({ ext: "png", base64: `${result.base64}` });
    }
  };

  return (
    <View style={styles.mainContainer}>
      <TouchableOpacity
        style={styles.iconContainer}
        onPress={() => navigation.goBack()}
      >
        <AntDesign name="left" style={styles.icon} />
      </TouchableOpacity>
      {role_id === 3 && (
        <View style={{ marginTop: 20 }}>
          <View style={styles.textContainer}>
            <Text style={styles.text}>I'm signing up as a Drugstore</Text>
          </View>
          <View>
            <TextInput
              style={styles.inputBox}
              autoCorrect={false}
              placeholder={"Drugstore Brand Name"}
              autoCapitalize={"none"}
              placeholderTextColor="#2065FE"
              selectionColor="#2065FE"
              enablesReturnKeyAutomatically={true}
              onChangeText={setBrand}
            />
            <View style={styles.textContainer}>
              <TouchableOpacity
                onPress={() => {
                  documentSelect();
                  setTimeout(() => {
                    setCommercialRecordAgree(true);
                  });
                }}
              >
                <Text style={styles.textAttach}>
                  Please Attach Your Commercial Record Pdf{" "}
                  <Text
                    onPress={() => {
                      pickImage();
                      setTimeout(() => {
                        setCommercialRecordAgree(true);
                      });
                    }}
                    style={styles.textAttachImage}
                  >
                    (Upload as image)
                  </Text>
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.agreeContainer}>
              <View
                style={{
                  borderWidth: Platform.OS === "ios" ? 1 : 0,
                  borderColor: "#969698",
                }}
              >
                <RadioButton
                  value="true"
                  status={agree === true ? "checked" : "unchecked"}
                  onPress={() => setAgree(!agree)}
                  color="#2065FE"
                />
              </View>
              <TouchableOpacity
                onPress={() =>
                  OpenAnything.Pdf("https://www.teeth-jo.com/pdf/contract.pdf")
                }
              >
                <Text style={styles.agreeText}>
                  {"  "}agree on the contract
                </Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={() => goSignUp()} style={styles.btn}>
              <Text style={styles.btnText}>Create Account</Text>
            </TouchableOpacity>
          </View>
        </View>
      )}

      {role_id === 2 && (
        <View style={{ marginTop: 20 }}>
          <Text style={styles.text}>I'm signing up as a Doctor</Text>
          <View style={{ marginTop: 30 }}>
            <TextInput
              style={styles.inputBox}
              autoCorrect={false}
              placeholder={"Specialized On"}
              autoCapitalize={"none"}
              placeholderTextColor="#2065FE"
              selectionColor="#2065FE"
              enablesReturnKeyAutomatically={true}
              onChangeText={setSpecialized}
            />
            <TextInput
              style={styles.inputBox}
              autoCorrect={false}
              placeholder={"Resident At"}
              autoCapitalize={"none"}
              placeholderTextColor="#2065FE"
              selectionColor="#2065FE"
              enablesReturnKeyAutomatically={true}
              onChangeText={setResident}
            />
            <View style={styles.textContainer}>
              <Text style={styles.text}>Please Attach</Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                setCertificateAgree(true);
                setTimeout(() => {
                  documentSelect();
                });
              }}
            >
              <Text style={styles.textAttachDoc}>
                Your Certificate Pdf{" "}
                <Text
                  onPress={() => {
                    setCertificateAgree(true);
                    setTimeout(() => {
                      pickImage();
                    });
                  }}
                  style={styles.textAttachImage}
                >
                  (Upload as image)
                </Text>
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                documentSelect();
                setNationalIDAgree(true);
              }}
            >
              <Text style={styles.textAttachDoc}>
                Your National Identity or Passport Pdf{" "}
                <Text
                  onPress={() => {
                    pickImage();
                    setNationalIDAgree(true);
                  }}
                  style={styles.textAttachImage}
                >
                  (Upload as image)
                </Text>
              </Text>
            </TouchableOpacity>
            <View style={styles.agreeContainer}>
              <View
                style={{
                  borderWidth: Platform.OS === "ios" ? 1 : 0,
                  borderColor: "#969698",
                }}
              >
                <RadioButton
                  value="true"
                  status={agree === true ? "checked" : "unchecked"}
                  onPress={() => setAgree(!agree)}
                  color="#2065FE"
                />
              </View>
              <TouchableOpacity
                onPress={() =>
                  OpenAnything.Pdf("https://www.teeth-jo.com/pdf/contract.pdf")
                }
              >
                <Text style={styles.agreeText}>
                  {"  "}agree on the contract
                </Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={() => goSignUp()} style={styles.btn}>
              <Text style={styles.btnText}>Create Account</Text>
            </TouchableOpacity>
          </View>
        </View>
      )}
    </View>
  );
};

export default ThirdRegister;

ThirdRegister.navigationOptions = () => {
  return {
    header: () => false,
  };
};

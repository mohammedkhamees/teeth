import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    paddingTop: 120,
    marginHorizontal: 15,
  },
  text: {
    color: "#2065FE",
    fontSize: 15,
    marginBottom: 10,
  },
  textAttach: {
    color: "#2065FE",
    fontSize: 13,
    marginBottom: 10,
    textDecorationLine: "underline",
  },
  textAttachDoc: {
    color: "#2065FE",
    fontSize: 15,
    marginBottom: 10,
    textDecorationLine: "underline",
    textAlign: "center",
  },
  textAttachImage: {
    color: "#2065FE",
    fontSize: 10,
    marginBottom: 10,
    textDecorationLine: "underline",
  },
  textContainer: {
    borderBottomColor: "#2065FE",
    borderBottomWidth: 1,
    marginBottom: 40,
    alignItems: "flex-start",
  },
  inputBox: {
    height: 30,
    borderBottomColor: "#2065FE",
    borderBottomWidth: 1,
    color: "blue",
    fontSize: 15,
    borderColor: "red",
    justifyContent: "center",
    marginBottom: 30,
  },
  icon: {
    fontSize: 20,
    color: "blue",
    fontWeight: "bold",
  },
  iconContainer: {
    position: "absolute",
    top: 80,
    left: 10,
  },
  btn: {
    width: "60%",
    alignItems: "center",
    backgroundColor: "#305FDF",
    padding: 10,
    borderWidth: 2,
    borderRadius: 28,
    borderColor: "white",
    alignSelf: "center",
    top: "90%",
  },
  btnText: { color: "#fff", fontSize: 20 },
  agreeText: { color: "#2065FE", fontSize: 13 },
  agreeContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 20,
  },
});

export default styles;

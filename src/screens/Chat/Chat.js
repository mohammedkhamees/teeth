// @refresh reset

import React, { useState, useCallback, useEffect } from "react";
import { GiftedChat } from "react-native-gifted-chat";
import {
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  Text,
  FlatList,
  Dimensions,
  ImageBackground,
  ActivityIndicator,
  Alert,
  Modal,
} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import axios from "axios";
import profilepc from "../../../assets/images/profilepc.png";
import * as firebase from "firebase";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCW_Hbgu39j0KMO0qNypb5gsXAP3_4O_j4",
  authDomain: "chat-6e589.firebaseapp.com",
  projectId: "chat-6e589",
  storageBucket: "chat-6e589.appspot.com",
  messagingSenderId: "510123698181",
  appId: "1:510123698181:web:9d249d4134af81ce52146c",
};

if (firebase.apps.length === 0) {
  firebase.initializeApp(firebaseConfig);
}

const { width, height } = Dimensions.get("window");

const Chat = ({ navigation }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [messages, setMessages] = useState([]);
  const [message, setMessage] = useState("");
  const [dbName, setDbName] = useState("");

  const userId = navigation.getParam("userId");
  const doctorName = navigation.getParam("doctorName");
  const doctorImg = navigation.getParam("doctorImg");
  const conversationId = navigation.getParam("conversationId");
  const userName = navigation.getParam("userName");
  const userEmail = navigation.getParam("email");

  useEffect(() => {
    setDbName(doctorName);
  });

  const db = firebase.firestore();
  const chatsRef = db.collection(`${conversationId}`);

  useEffect(() => {
    const unsubscribe = chatsRef.onSnapshot((querySnapshot) => {
      const messagesFirestore = querySnapshot
        .docChanges()
        .filter(({ type }) => type === "added")
        .map(({ doc }) => {
          const message = doc.data();
          return { ...message, createdAt: message.createdAt.toDate() };
        })
        .sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime());
      appendMessages(messagesFirestore);
    });
    return () => unsubscribe();
  }, [dbName]);

  const appendMessages = useCallback(
    (messages) => {
      setMessages((previousMessages) =>
        GiftedChat.append(previousMessages, messages)
      );
    },
    [messages]
  );

  async function handleSend(messages) {
    const writes = messages.map((m) => chatsRef.add(m));
    await Promise.all(writes);
  }

  return (
    <View style={{ flex: 1 }}>
      <Modal
        statusBarTranslucent={true}
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <TouchableOpacity
          style={styles.centeredView}
          onPressOut={() => {
            setModalVisible(false);
          }}
        >
          <View style={styles.modalView}>
            <TouchableOpacity
              onPress={() => {
                setModalVisible(false);
              }}
              style={{
                alignSelf: "center",
                justifyContent: "center",
                borderBottomWidth: 1,
                width: "100%",
              }}
            >
              <Text
                style={{
                  alignSelf: "center",
                  justifyContent: "center",
                  padding: 10,
                  fontSize: 22,
                }}
              >
                Delete
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                setModalVisible(false);
                navigation.navigate("report", { userId });
              }}
              style={{
                alignSelf: "center",
                justifyContent: "center",
                width: "100%",
              }}
            >
              <Text
                style={{
                  alignSelf: "center",
                  justifyContent: "center",
                  padding: 10,
                  fontSize: 22,
                }}
              >
                Report
              </Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </Modal>

      <View
        style={{
          justifyContent: "center",
          backgroundColor: "#004FFE",
          height: "12%",
          borderBottomEndRadius: 20,
          borderBottomLeftRadius: 20,
          paddingTop: 10,
        }}
      >
        <View
          style={{
            marginTop: 20,
            marginRight: 30,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <TouchableOpacity
            onPress={() => {
              setMessages([]);
              navigation.goBack();
            }}
            style={{ flex: 0.1 }}
          >
            <FontAwesome5 active size={30} name={"angle-left"} color="white" />
          </TouchableOpacity>

          <View style={{ flex: 0.8, alignItems: "center" }}>
            <Text style={{ color: "#fff", fontSize: 20 }}>Chat</Text>
          </View>
        </View>
      </View>

      <View
        style={{
          flexDirection: "row",
          borderBottomWidth: 1,
          borderBottomColor: "#6164F0",
          marginHorizontal: 20,
        }}
      >
        <TouchableOpacity>
          <Image source={{ uri: doctorImg }} style={styles.profilePicture} />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            flex: 0.5,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Text
            style={{
              color: "#6164F0",
              fontSize: 22,
            }}
          >
            {doctorName}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{
            position: "absolute",
            right: "0%",
            top: "50%",
          }}
          onPress={() => setModalVisible(true)}
        >
          <FontAwesome5 active size={22} name={"ellipsis-v"} color="#2BA5BE" />
        </TouchableOpacity>
      </View>

      <GiftedChat
        messages={messages}
        user={{ user: userName, _id: userEmail }}
        onSend={handleSend}
      />
    </View>
  );
};

export default Chat;

Chat.navigationOptions = () => {
  return {
    header: () => false,
  };
};

const styles = StyleSheet.create({
  profilePicture: {
    width: height / 10,
    height: height / 10,
    resizeMode: "cover",
    borderRadius: height / 6,
    marginLeft: 30,
    marginVertical: 15,
  },
  text: {
    fontSize: 13,
    color: "#fff",
    marginBottom: 10,
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    fontSize: 20,
  },
  modalView: {
    width: "70%",
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  mainContainer: {
    flex: 1,
  },

  flatlistContainer: {
    flex: 10 / 9,
  },
});

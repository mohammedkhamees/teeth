import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Modal,
  Alert,
  FlatList,
} from "react-native";
import { AirbnbRating } from "react-native-elements";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import styles from "./style";
import axios from "axios";

const DoctorItem = ({
  email,
  drItem,
  navigation,
  token,
  doctor_id,
  doctor_name,
  doctor_image,
  collection,
  role,
}) => {
  const [modalFolderVisible, setModalFolderVisible] = useState(false);
  const [modalCollectionVisible, setModalCollectionVisible] = useState(false);

  const addToCollection = (id) => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/favorite/doctor/${doctor_id}/folder/${id}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        Alert.alert("Add to Collection", "You a Collection Succussfully");
      })
      .catch((error) => {
        console.log("addToCollection error", error);
      });
  };

  return (
    <View style={styles.itemContainer}>
      <Modal
        statusBarTranslucent={true}
        animationType="slide"
        transparent={true}
        visible={modalFolderVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalFolderVisible(!modalFolderVisible);
        }}
      >
        <TouchableOpacity
          style={styles.centeredView}
          onPressOut={() => {
            setModalFolderVisible(false);
          }}
        >
          <View style={styles.modalView}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate("AddSaved", {
                  doctor_id,
                });
                setModalFolderVisible(false);
              }}
              style={{ borderBottomWidth: 1, width: "90%" }}
            >
              <Text style={styles.modalText}>Create New Collection</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setModalCollectionVisible(true);
                setModalFolderVisible(false);
              }}
              style={{ borderBottomWidth: 1, width: "90%" }}
            >
              <Text
                style={{
                  alignSelf: "center",
                  justifyContent: "center",
                  padding: 10,
                  fontSize: 20,
                }}
              >
                Add to a Collection
              </Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </Modal>

      {/* collections modal */}
      <Modal
        statusBarTranslucent={true}
        animationType="slide"
        transparent={true}
        visible={modalCollectionVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalCollectionVisible(!modalCollectionVisible);
        }}
      >
        <TouchableOpacity
          style={styles.centeredView}
          onPressOut={() => {
            setModalCollectionVisible(false);
          }}
        >
          <View style={styles.modalView}>
            <TouchableOpacity
              onPress={() => setModalCollectionVisible(false)}
              style={{ borderBottomWidth: 1, width: "90%" }}
            >
              <Text style={styles.modalText}>Collection</Text>
            </TouchableOpacity>

            <FlatList
              data={collection}
              style={{ width: "90%" }}
              numColumns={1}
              renderItem={({ item }) => (
                <TouchableOpacity
                  onPress={() => {
                    setModalCollectionVisible(false);
                    addToCollection(item.id);
                  }}
                  style={{
                    alignSelf: "center",
                    justifyContent: "center",
                    borderBottomWidth: 1,
                    width: "100%",
                  }}
                >
                  <Text
                    style={{
                      alignSelf: "center",
                      justifyContent: "center",
                      padding: 10,
                    }}
                  >
                    {item.folder_name}
                  </Text>
                </TouchableOpacity>
              )}
              keyExtractor={(item) => item.id}
            />
          </View>
        </TouchableOpacity>
      </Modal>

      <TouchableOpacity
        onPress={() =>
          navigation.navigate("Profile", {
            name: doctor_name,
            image: doctor_image
              ? doctor_image
              : "https://png.pngtree.com/png-clipart/20190904/original/pngtree-circular-pattern-user-cartoon-avatar-png-image_4492893.jpg",
            drItem: drItem,
          })
        }
        style={styles.textTouchable}
      >
        <Image
          source={
            doctor_image
              ? {
                  uri: `${doctor_image}`,
                }
              : {
                  uri: "https://png.pngtree.com/png-clipart/20190904/original/pngtree-circular-pattern-user-cartoon-avatar-png-image_4492893.jpg",
                }
          }
          style={styles.profilePictureDoctors}
          resizeMode="contain"
          PlaceholderContent={<ActivityIndicator />}
        />

        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "cente",
            width: "100%",
          }}
        >
          <AirbnbRating
            showRating={false}
            count={5}
            defaultRating={1 != 0 ? 4 : 0}
            isDisabled={true}
            size={14}
            reviewColor="#f1c40f"
          />

          <TouchableOpacity
            style={{ position: "absolute", right: "4%" }}
            onPress={() => role === 4 && setModalFolderVisible(true)}
          >
            <FontAwesome5 active size={20} name={"bookmark"} color="#aaa" />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
      <Text
        style={{
          alignSelf: "center",
          justifyContent: "center",
          padding: 10,
          color: "black",
        }}
      >
        {doctor_name}
      </Text>
    </View>
  );
};

export default DoctorItem;

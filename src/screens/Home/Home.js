// @refresh reset
import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Modal,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  FlatList,
  Dimensions,
  Image,
  ActivityIndicator,
} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { AirbnbRating, colors, Divider } from "react-native-elements";
import profilepc from "../../../assets/images/profilepc.png";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import DoctorItem from "./DoctorItem";
import styles from "./style";

const { width, height } = Dimensions.get("window");

const Home = ({ navigation }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [modalCitiesVisible, setModalCitiesVisible] = useState(false);
  const [modalAddressesVisible, setModalAddressesVisible] = useState(false);
  const [token, setToken] = useState("");
  const [role, setRole] = useState(0);
  const [doctorData, setDoctorData] = useState([]);
  const [doctorDataFiltered, setDoctorDataFiltered] = useState([]);
  const [best, setBest] = useState([]);
  const [number, setNumber] = useState(0);
  const [cities, setCities] = useState([]);
  const [addresses, setAddresses] = useState([]);
  const [city_id, setCity_id] = useState("");
  const [collection, setCollection] = useState([]);

  const IMAGEST = [
    "https://familydoctor.org/wp-content/uploads/2018/02/41808433_l-705x470.jpg",
    "https://upload.wikimedia.org/wikipedia/commons/8/8e/G%C3%A9rald_KIERZEK_%28Cr%C3%A9dit_Ibo%29.jpg",
    "https://www.kauveryhospital.com/doctorimage/recent/DrVMuraliMageshanaesthesiology2019-02-18%2011:44:09am.jpg",
    "https://myteledoc.app/wp-content/uploads/2020/09/happy-young-female-doctor-smiling-and-looking-at-c-WDEKYYG.jpg",
  ];

  useEffect(() => {
    getToken();
  }, []);

  const getToken = async () => {
    const token = JSON.parse(await AsyncStorage.getItem("token"));
    const role = JSON.parse(await AsyncStorage.getItem("role"));
    if (token) {
      setToken(token);
      getDoctor(token);
      getBest(token);
      getCities(token);
      setRole(role);
      if (role === 4) getFolders(token);
    } else {
      console.log("ERROR GET TOKEN");
    }
  };

  const getFolders = (token) => {
    axios
      .get(`https://nextstageksa.com/clinic/api/favorite`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        setCollection([...response.data.data]);
      })
      .catch((error) => {
        console.log("getFoldersFromHome error", error);
      });
  };

  const getDoctor = (token, cityId) => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/Doctor/all?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        { token }
      )
      .then((response) => {
        if (!cityId && cityId !== 0) {
          setDoctorData([...response.data.Data.Doctor]);
          setDoctorDataFiltered([]);
        } else {
          const filteredDocs = response.data.Data.Doctor.filter(
            (doctor) => doctor.city_id === cityId
          );
          if (filteredDocs.length) {
            setDoctorDataFiltered([...filteredDocs]);
          }
        }
      })
      .catch((error) => {
        console.log("getDoctor error", error.message);
      });
  };

  const getBest = (token) => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/bestDoctor?key=6Q4AE52F15F8B82735E9485CA3EC7&`,
        { token }
      )
      .then((response) => {
        setBest(response.data.BestDoctor.User);
      })
      .catch((error) => {
        console.log("getBest error", error.message);
      });
  };

  const getCities = (token) => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/City/all?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        { token }
      )
      .then((response) => {
        setCities([...response.data.Data.City]);
      })
      .catch((error) => {
        console.log("getCities error", error.message);
      });
  };

  const getAddresses = (cityId) => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/FindDistrict?key=6Q4AE52F15F8B82735E9485CA3EC7&id_city=${cityId}`,
        { token }
      )
      .then((response) => {
        setAddresses([...response.data.Data.Loctions]);
        setModalAddressesVisible(true);
      })
      .catch((error) => {
        console.log("getAddresses error", error.message);
      });
  };

  const getCityId = (city) => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/CityAarry?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        { token }
      )
      .then((response) => {
        setCity_id(response.data.Data.Loctions[city]);
        getAddresses(response.data.Data.Loctions[city]);
      })
      .catch((error) => {
        console.log("getCityId error", error.message);
      });
  };

  const searchDoctor = (name) => {
    setDoctorDataFiltered(
      doctorData.filter((doctor) => doctor.name.includes(name))
    );
  };

  const imgSwapPrev = () => {
    if (number === 0) {
      setNumber(best.length - 1);
    } else {
      setNumber(number - 1);
    }
  };

  const imgSwapNext = () => {
    if (number === best.length - 1) {
      setNumber(0);
    } else {
      setNumber(number + 1);
    }
  };

  return (
    <View style={styles.container}>
      <Modal
        statusBarTranslucent={true}
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <TouchableOpacity
          style={styles.centeredView}
          onPressOut={() => {
            setModalVisible(false);
          }}
        >
          <View style={styles.modalView}>
            <TouchableOpacity
              onPress={() => {
                setModalVisible(false);
                getDoctor(token);
              }}
              style={{
                alignSelf: "center",
                justifyContent: "center",
                borderBottomWidth: 1,
                width: "100%",
              }}
            >
              <Text
                style={{
                  alignSelf: "center",
                  justifyContent: "center",
                  padding: 10,
                  fontSize: 20,
                }}
              >
                {" "}
                Show All{" "}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                setModalVisible(false);
                setModalCitiesVisible(true);
              }}
              style={{
                alignSelf: "center",
                justifyContent: "center",
                borderBottomWidth: 1,
                width: "100%",
              }}
            >
              <Text
                style={{
                  alignSelf: "center",
                  justifyContent: "center",
                  padding: 10,
                  fontSize: 20,
                }}
              >
                By Location
              </Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </Modal>
      {/* cities modal */}
      <Modal
        statusBarTranslucent={true}
        animationType="slide"
        transparent={true}
        visible={modalCitiesVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalCitiesVisible(!modalCitiesVisible);
        }}
      >
        <TouchableOpacity
          style={styles.centeredView}
          onPressOut={() => {
            setModalCitiesVisible(false);
          }}
        >
          <View style={styles.modalView}>
            <TouchableOpacity
              onPress={() => setModalCitiesVisible(false)}
              style={{ borderBottomWidth: 1, width: "90%" }}
            >
              <Text style={styles.modalText}> City </Text>
            </TouchableOpacity>

            <FlatList
              data={cities}
              style={{ width: "90%" }}
              numColumns={1}
              renderItem={({ item }) => (
                <TouchableOpacity
                  onPress={() => {
                    setModalCitiesVisible(false);
                    getCityId(item.name);
                  }}
                  style={{
                    alignSelf: "center",
                    justifyContent: "center",
                    borderBottomWidth: 1,
                    width: "100%",
                  }}
                >
                  <Text
                    style={{
                      alignSelf: "center",
                      justifyContent: "center",
                      padding: 10,
                    }}
                  >
                    {item.name}
                  </Text>
                </TouchableOpacity>
              )}
              keyExtractor={(item) => item.name}
            />
          </View>
        </TouchableOpacity>
      </Modal>

      {/* addresses modal */}
      <Modal
        statusBarTranslucent={true}
        animationType="slide"
        transparent={true}
        visible={modalAddressesVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalAddressesVisible(!modalAddressesVisible);
        }}
      >
        <TouchableOpacity
          style={styles.centeredView}
          onPressOut={() => {
            setModalAddressesVisible(false);
          }}
        >
          <View style={styles.modalView}>
            <TouchableOpacity
              onPress={() => setModalAddressesVisible(false)}
              style={{ borderBottomWidth: 1, width: "90%" }}
            >
              <Text style={styles.modalText}> Address </Text>
            </TouchableOpacity>

            <FlatList
              data={addresses}
              style={{ width: "90%" }}
              numColumns={1}
              renderItem={({ item }) => (
                <TouchableOpacity
                  onPress={() => {
                    setModalAddressesVisible(false);
                    getDoctor(token, city_id);
                  }}
                  style={{
                    alignSelf: "center",
                    justifyContent: "center",
                    borderBottomWidth: 1,
                    width: "100%",
                  }}
                >
                  <Text
                    style={{
                      alignSelf: "center",
                      justifyContent: "center",
                      padding: 10,
                    }}
                  >
                    {item.name}
                  </Text>
                </TouchableOpacity>
              )}
              keyExtractor={(item) => item.name}
            />
          </View>
        </TouchableOpacity>
      </Modal>

      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <View
          style={{
            width: "100%",
            height: "52%",
            backgroundColor: "#004FFE",
            alignItems: "center",
          }}
        >
          <View style={{ height: "1%" }} />

          <View
            style={{
              height: 60,
              width: "88%",
              justifyContent: "center",
              marginTop: 20,
            }}
          >
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
              <FontAwesome5
                active
                size={20}
                name={"align-justify"}
                color="#fff"
              />
            </TouchableOpacity>
          </View>

          <View
            style={{
              width: "90%",
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "row",
            }}
          >
            <View style={{ alignItems: "center", alignItems: "center" }}>
              <TouchableOpacity onPress={() => setModalVisible(true)}>
                <FontAwesome5 active size={25} name={"filter"} color="white" />
              </TouchableOpacity>
            </View>

            <View style={{ flex: 0.9, alignItems: "center" }}>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  backgroundColor: "#fff",
                  borderRadius: 30,
                }}
              >
                <TextInput
                  inlineImageLeft="user"
                  inlineImagePadding={5}
                  style={styles.inputBox}
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                  placeholder={"  Search"}
                  placeholderTextColor="#768AE1"
                  selectionColor="#004FFE"
                  keyboardType="email-address"
                  enablesReturnKeyAutomatically={true}
                  onChangeText={(name) => {
                    searchDoctor(name);
                  }}
                />

                <FontAwesome5
                  style={{ padding: 5 }}
                  active
                  size={20}
                  name={"search"}
                  color="#004FFE"
                />
              </View>
            </View>
          </View>

          <View style={{ padding: 10 }}>
            <TouchableOpacity onPress={() => console.log(doctorData)}>
              <Text style={{ fontSize: 20, color: "#fff" }}>
                The Best Doctor
              </Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              alignSelf: "center",
            }}
          >
            <TouchableOpacity
              onPress={() => imgSwapPrev()}
              style={{
                flex: 0.2,
                justifyContent: "center",
                alignSelf: "center",
              }}
            >
              <FontAwesome5
                style={{ padding: 10, marginLeft: "33%" }}
                active
                size={30}
                name={"angle-left"}
                color="white"
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={{ flex: 0.6, alignSelf: "center" }}
              onPress={() => console.log("DOCTOR DATA")}
            >
              <Image
                source={{ uri: best[number]?.profile_image }}
                style={styles.profilePicture}
                resizeMode="contain"
                PlaceholderContent={<ActivityIndicator />}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => imgSwapNext()}
              style={{
                flex: 0.2,
                justifyContent: "center",
                alignSelf: "center",
              }}
            >
              <FontAwesome5
                style={{ padding: 10, marginLeft: "33%" }}
                active
                size={30}
                name={"angle-right"}
                color="white"
              />
            </TouchableOpacity>
          </View>

          <View style={{ padding: 7 }}>
            <Text style={{ color: "#fff" }}>
              _________________________________________________
            </Text>
          </View>

          <View style={{ flexDirection: "row", padding: 5 }}>
            <TouchableOpacity
              onPress={() => getDoctor()}
              style={{ flex: 0.5, alignItems: "center" }}
            >
              <Text style={{ fontSize: 20, color: "#fff" }}> Doctors </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => navigation.navigate("pharmacies")}
              style={{ flex: 0.5, alignItems: "center" }}
            >
              <Text style={{ fontSize: 20, color: "#CFD0D5" }}>Pharmacies</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View
          style={{
            backgroundColor: "#fff",
            height: "48%",
            width: "100%",
            alignItems: "center",
          }}
        >
          <View style={styles.mainContainer}>
            <View style={styles.flatlistContainer}>
              <FlatList
                data={
                  doctorDataFiltered?.length ? doctorDataFiltered : doctorData
                }
                numColumns={2}
                renderItem={({ item }) => (
                  <DoctorItem
                    email={item.email}
                    drItem={item}
                    navigation={navigation}
                    token={token}
                    doctor_id={item.id}
                    doctor_name={item.name}
                    doctor_image={item.profile_image}
                    collection={collection}
                    role={role}
                  />
                )}
                keyExtractor={(item) => item.email}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Home;

Home.navigationOptions = () => {
  return {
    header: () => false,
  };
};

import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  Image,
  Modal,
  TextInput,
  TouchableOpacity,
  Text,
  FlatList,
  Dimensions,
  Alert,
} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import profilepc from "../../../assets/images/profilepc.png";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";

const { width, height } = Dimensions.get("window");

const pharmacies = ({ navigation }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [modalCitiesVisible, setModalCitiesVisible] = useState(false);
  const [modalAddressesVisible, setModalAddressesVisible] = useState(false);
  const [token, setToken] = useState("");
  const [pharms, setPharms] = useState([]);
  const [cities, setCities] = useState([]);
  const [addresses, setAddresses] = useState([]);
  const [city_id, setCity_id] = useState("");
  const [meds, setMeds] = useState([]);
  const [filteredPharms, setFilteredPharms] = useState([]);

  useEffect(() => {
    getToken();
  }, []);

  const getToken = async () => {
    const token = await AsyncStorage.getItem("token");
    if (token) {
      setToken(token);
      getPharm(token);
      //getMeds(token);
    } else {
      console.log("ERROR GET TOKEN");
    }
  };

  const getPharm = (token, cityId) => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/Pharmcy/all?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        { token }
      )
      .then((response) => {
        if (!cityId) {
          setPharms([...response.data.Data.Pharmacy]);
          setFilteredPharms([]);
        } else
          setFilteredPharms(
            response.data.Data.Pharmacy.filter(
              (pharm) => pharm.city_id === cityId
            )
          );
      })
      .catch((error) => {
        console.log("error", error);
      });
  };

  const getPharmByDiscount = (discount) => {
    const filteredMeds = meds.filter((Med) => Med.discount >= discount);

    if (filteredMeds.length > pharms.length) {
      setFilteredPharms(
        filteredMeds.filter(
          (med, index) => pharms[index]?.keyPhar === med.keyPhar
        )
      );
    } else {
      setFilteredPharms(
        pharms.filter(
          (pharm, index) => filteredMeds[index]?.keyPhar === pharm.keyPhar
        )
      );
    }
  };

  const getPharmByName = (name) => {
    setFilteredPharms([
      ...pharms.filter((pharm, index) => pharm.name.includes(name)),
    ]);
  };

  const getMeds = (token, discount) => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/allMedicine?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        { token }
      )
      .then((response) => {
        setMeds([...response.data.Data.Medicine]);
      })
      .catch((error) => {
        console.log("error", error.message);
      });
  };

  const getCities = (token) => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/City/all?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        { token }
      )
      .then((response) => {
        setCities([...response.data.Data.City]);
      })
      .catch((error) => {
        console.log("error", error.message);
      });
  };

  const getAddresses = (cityId) => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/FindDistrict?key=6Q4AE52F15F8B82735E9485CA3EC7&id_city=${cityId}`,
        { token }
      )
      .then((response) => {
        setAddresses([...response.data.Data.Loctions]);
        setModalAddressesVisible(true);
      })
      .catch((error) => {
        console.log("error", error.message);
      });
  };

  const getCityId = (city) => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/CityAarry?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        { token }
      )
      .then((response) => {
        setCity_id(response.data.Data.Loctions[city]);
        getAddresses(response.data.Data.Loctions[city]);
      })
      .catch((error) => {
        console.log("error", error.message);
      });
  };

  const Producto = ({ equipo, nombre, city_id }) => {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.itemContainer}>
          <View style={styles.item_LogoContainer}>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate("profilepharmacieds", {
                  key_phar: nombre,
                  name: equipo,
                  cityId: city_id,
                })
              }
            >
              <Image
                source={{
                  uri: "https://png.pngtree.com/png-clipart/20190924/original/pngtree-user-vector-avatar-png-image_4830521.jpg",
                }}
                style={{ width: 140, height: 140 }}
              ></Image>
            </TouchableOpacity>
          </View>

          <View style={styles.item_DescriptionContainer}>
            <Text style={{ fontSize: 20, fontWeight: "bold" }}>{equipo}</Text>
          </View>
        </View>
      </View>
    );
  };

  return (
    <View style={{ flex: 1 }}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <TouchableOpacity
              style={{
                borderBottomWidth: 1,
                width: "90%",
              }}
              onPress={() => {
                setModalVisible(false);
                getPharm(token);
              }}
            >
              <Text
                style={{
                  alignSelf: "center",
                  justifyContent: "center",
                  padding: 10,
                }}
              >
                {" "}
                Show All{" "}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                borderBottomWidth: 1,
                width: "90%",
              }}
              onPress={() => {
                setModalVisible(false);
                setModalCitiesVisible(true);
                getCities(token);
              }}
            >
              <Text
                style={{
                  alignSelf: "center",
                  justifyContent: "center",
                  padding: 10,
                }}
              >
                {" "}
                By Location{" "}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                borderBottomWidth: 1,
                width: "90%",
              }}
              onPress={() => {
                setModalVisible(false);
                getPharmByDiscount(50);
              }}
            >
              <Text
                style={{
                  alignSelf: "center",
                  justifyContent: "center",
                  padding: 10,
                }}
              >
                Up To 50%{" "}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                borderBottomWidth: 1,
                width: "90%",
              }}
              onPress={() => {
                setModalVisible(false);
                getPharmByDiscount(0.1);
              }}
            >
              <Text
                style={{
                  alignSelf: "center",
                  justifyContent: "center",
                  padding: 10,
                }}
              >
                {" "}
                All Offers{" "}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      {/* cities modal */}
      <Modal
        statusBarTranslucent={true}
        animationType="slide"
        transparent={true}
        visible={modalCitiesVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalCitiesVisible(!modalCitiesVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <TouchableOpacity
              onPress={() => setModalCitiesVisible(false)}
              style={{ borderBottomWidth: 1, width: "90%" }}
            >
              <Text style={styles.modalText}> City </Text>
            </TouchableOpacity>

            <FlatList
              data={cities}
              style={{ width: "90%" }}
              numColumns={1}
              renderItem={({ item }) => (
                <TouchableOpacity
                  onPress={() => {
                    setModalCitiesVisible(false);
                    getCityId(item.name);
                  }}
                  style={{
                    alignSelf: "center",
                    justifyContent: "center",
                    borderBottomWidth: 1,
                    width: "100%",
                  }}
                >
                  <Text
                    style={{
                      alignSelf: "center",
                      justifyContent: "center",
                      padding: 10,
                    }}
                  >
                    {item.name}
                  </Text>
                </TouchableOpacity>
              )}
              keyExtractor={(item) => item.name}
            />
          </View>
        </View>
      </Modal>

      {/* addresses modal */}
      <Modal
        statusBarTranslucent={true}
        animationType="slide"
        transparent={true}
        visible={modalAddressesVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalAddressesVisible(!modalAddressesVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <TouchableOpacity
              onPress={() => setModalAddressesVisible(false)}
              style={{ borderBottomWidth: 1, width: "90%" }}
            >
              <Text style={styles.modalText}> Address </Text>
            </TouchableOpacity>

            <FlatList
              data={addresses}
              style={{ width: "90%" }}
              numColumns={1}
              renderItem={({ item }) => (
                <TouchableOpacity
                  onPress={() => {
                    setModalAddressesVisible(false);
                    getPharm(token, city_id);
                  }}
                  style={{
                    alignSelf: "center",
                    justifyContent: "center",
                    borderBottomWidth: 1,
                    width: "100%",
                  }}
                >
                  <Text
                    style={{
                      alignSelf: "center",
                      justifyContent: "center",
                      padding: 10,
                    }}
                  >
                    {item.name}
                  </Text>
                </TouchableOpacity>
              )}
              keyExtractor={(item) => item.name}
            />
          </View>
        </View>
      </Modal>

      <TouchableOpacity
        onPress={() => navigation.openDrawer()}
        style={{
          justifyContent: "center",
          backgroundColor: "#004FFE",
          height: "12%",
          borderBottomEndRadius: 20,
          borderBottomLeftRadius: 20,
          width: "100%",
        }}
      >
        <FontAwesome5
          active
          size={20}
          name={"align-justify"}
          color="white"
          style={{ paddingLeft: 30, paddingTop: 30 }}
        />
      </TouchableOpacity>

      <View style={{ flexDirection: "row", height: 70, padding: 20 }}>
        <TouchableOpacity
          onPress={() => navigation.navigate("Home")}
          style={{ flex: 0.5, alignItems: "center" }}
        >
          <Text style={{ fontSize: 20, color: "#6E7ED1" }}> Doctors </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{ flex: 0.5, alignItems: "center" }}
          onPress={() => navigation.navigate("pharmacies")}
        >
          <Text style={{ fontSize: 20, color: "#004FFE" }}> Pharmacies </Text>
        </TouchableOpacity>
      </View>
      <View
        style={{
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "row",
        }}
      >
        <TouchableOpacity
          onPress={() => setModalVisible(true)}
          style={{ alignItems: "center", alignItems: "center" }}
        >
          <FontAwesome5 active size={25} name={"filter"} color="#004FFE" />
        </TouchableOpacity>

        <View style={{ flex: 0.9, alignItems: "center" }}>
          <TextInput
            style={styles.inputBox}
            autoCorrect={false}
            placeholder={"  Search by name"}
            autoCapitalize={"none"}
            placeholderTextColor="#004FFE"
            selectionColor="#004FFE"
            keyboardType="email-address"
            enablesReturnKeyAutomatically={true}
            onChangeText={(name) => {
              getPharmByName(name);
            }}
          />
        </View>
      </View>

      <View style={styles.mainContainer}>
        <View style={styles.flatlistContainer}>
          <FlatList
            data={filteredPharms.length ? filteredPharms : pharms}
            numColumns={2}
            renderItem={({ item }) => (
              <Producto
                equipo={item.name}
                nombre={item.key_phar}
                city_id={item.city_id}
              ></Producto>
            )}
            keyExtractor={(item) => item.email}
          />
        </View>
      </View>
    </View>
  );
};

export default pharmacies;

pharmacies.navigationOptions = () => {
  return {
    header: () => false,
  };
};

const styles = StyleSheet.create({
  inputBox: {
    width: "90%",
    height: 35,
    backgroundColor: "white",
    color: "#004FFE",
    fontSize: 15,
    borderWidth: 1,
    borderColor: "#004FFE",
    borderRadius: 35,
    padding: 10,
  },
  profilePictureWrapper: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 1,
  },
  profilePicture: {
    width: height / 4.1,
    height: height / 4.1,
    resizeMode: "cover",
    borderRadius: height / 6,
  },
  profilePictureDoctors: {
    width: height / 4,
    height: height / 4,
    resizeMode: "cover",
    borderRadius: height / 6,
    padding: 1,
  },
  imagesGroup: {
    flexWrap: "wrap",
    margin: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    fontSize: 13,
    color: "#fff",
    marginBottom: 10,
  },
  mainContainer: {
    flex: 1,
  },

  flatlistContainer: {
    flex: 10 / 9,
  },
  itemContainer: {
    justifyContent: "center",
    alignItems: "center",

    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
  },

  item_LogoContainer: {
    flex: 1 / 2,
    justifyContent: "center",
    alignItems: "center",
  },

  item_DescriptionContainer: {
    flex: 1 / 3,
    justifyContent: "center",
    alignItems: "center",

    marginTop: 5,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    width: "70%",
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 10,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#004FFE",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 25,
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    fontSize: 20,
  },
});

import { StyleSheet, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  profilePictureDoctors: {
    width: height / 7.5,
    height: height / 7.5,
    resizeMode: "cover",
    borderRadius: height / 6,
    padding: 1,
  },

  itemContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    padding: 5,
  },

  textTouchable: {
    width: 130,
    alignItems: "center",
    height: 160,
    borderColor: "#004FFE",
    borderWidth: 1,
    borderTopStartRadius: 80,
    borderTopRightRadius: 80,
    borderBottomEndRadius: 10,
    borderBottomLeftRadius: 10,
    margin: 20,
  },

  text: { fontWeight: "bold", marginTop: 4, color: "#98989D" },

  container: { flex: 1 },
  inputBox: {
    width: "90%",
    height: 35,
    borderBottomColor: "#004FFE",
    backgroundColor: "#fff",
    color: "#004FFE",
    fontSize: 15,
    borderRadius: 35,
    padding: 10,
  },
  profilePictureWrapper: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 1,
  },
  profilePicture: {
    width: height / 4.1,
    height: height / 4.1,
    resizeMode: "cover",
    borderRadius: height / 4,
    alignSelf: "center",
  },
  profilePictureDoctors: {
    width: height / 7.5,
    height: height / 7.5,
    resizeMode: "cover",
    borderRadius: height / 6,
    padding: 1,
  },
  imagesGroup: {
    flexWrap: "wrap",
    margin: 5,
    alignItems: "center",
    justifyContent: "center",
  },

  mainContainer: {
    flex: 1,
  },

  flatlistContainer: {
    flex: 10 / 9,
  },
  itemContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
  },

  item_LogoContainer: {
    flex: 1 / 2,
    justifyContent: "center",
    alignItems: "center",
  },

  item_DescriptionContainer: {
    flex: 1 / 3,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 5,
  },

  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    width: "70%",
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 10,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#004FFE",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 25,
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    fontSize: 20,
  },
});

export default styles;

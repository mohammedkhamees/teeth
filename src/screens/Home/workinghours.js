import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  Text,
  Button,
  FlatList,
  Dimensions,
  Picker,
} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import RadioButtonRN from "radio-buttons-react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";

const workinghours = ({ navigation }) => {
  const [checked, setChecked] = useState("");
  const [time, setTime] = useState([]);
  const [Add, setAdd] = useState(false);
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");
  const [workingDay, setWorkingDay] = useState([]);
  const [token, setToken] = useState("");

  useEffect(() => {
    getToken();
  }, []);

  const getToken = async () => {
    const token = JSON.parse(await AsyncStorage.getItem("token"));
    if (token) {
      setToken(token);
      getWorkDayHours(token);
    } else {
      console.log("ERROR GET TOKEN");
    }
  };

  const getWorkDayHours = (token) => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/GetSavenDate?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        setWorkingDay([...response.data]);
      })
      .catch((error) => {
        console.log("ERROR getWorkingHours: ", error);
      });
  };

  const submitDay = (item) => {
    setChecked(item.day);
    getTime(item.times);
  };

  const getTime = (time) => {
    setTime([...time]);
  };

  const submitTime = () => {
    axios
      .post(
        `https://nextstageksa.com/clinic/api/SetSavenDate?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        { to: `${to}:00`, from: `${from}:00`, day: checked },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        setChecked("");
        setTime([]);
        setAdd(false);
        setTo("");
        setFrom("");
        getWorkDayHours(token);
      })
      .catch((error) => {
        console.log("ERROR getWorkingHours: ", error);
      });
  };

  return (
    <View style={{ flex: 1, width: "100%" }}>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "#004FFE",
          height: "12%",
          borderBottomEndRadius: 20,
          borderBottomLeftRadius: 20,
          paddingTop: 20,
        }}
      >
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <FontAwesome5 active size={30} name={"angle-left"} color="white" />
        </TouchableOpacity>

        <View
          style={{
            flex: 0.8,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Text style={{ color: "#fff", fontSize: 20 }}>Working Hours</Text>
        </View>
      </View>

      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
          height: "20%",
          marginTop: 30,
          width: "100%",
          marginHorizontal: 20,
        }}
      >
        <FlatList
          data={workingDay}
          numColumns={3}
          renderItem={({ item }) => (
            <View
              style={{
                padding: 5,
                width: "30%",
              }}
            >
              <TouchableOpacity
                onPress={() => submitDay(item)}
                style={{
                  backgroundColor: checked === item.day ? "#004FFE" : "#CECED4",
                  padding: 10,
                  borderRadius: 22,
                }}
              >
                <Text
                  style={{
                    fontSize: 18,
                    textAlign: "center",
                    color: "white",
                    fontWeight: "bold",
                  }}
                >
                  {item.day}
                </Text>
              </TouchableOpacity>
            </View>
          )}
          keyExtractor={(item) => item.day}
        />
      </View>

      {checked ? (
        <View style={{ backgroundColor: "#004FFE", flex: 1 }}>
          <View style={{ backgroundColor: "#004FFE" }}>
            <FlatList
              data={time}
              horizontal={false}
              renderItem={({ item }) => (
                <View
                  style={{
                    padding: 5,
                    borderBottomWidth: 1,
                    borderBottomColor: "#fff",
                    marginHorizontal: "5%",
                  }}
                >
                  <Text
                    style={{
                      color: "#fff",
                      padding: 4,
                      alignSelf: "center",
                      fontWeight: "bold",
                      fontSize: 18,
                    }}
                  >
                    {item.display_time}
                  </Text>
                </View>
              )}
              keyExtractor={(item) => item.day}
            />

            <TouchableOpacity
              onPress={() => setAdd(true)}
              style={{
                borderBottomWidth: 1,
                borderBottomColor: "#fff",
                alignSelf: "center",
                marginTop: 10,
              }}
            >
              <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 18 }}>
                Add New
              </Text>
            </TouchableOpacity>
          </View>
          {Add ? (
            <View style={styles.container}>
              <View style={styles.button}>
                <Picker
                  selectedValue={from}
                  style={{
                    height: 50,
                    width: 100,
                    color: "#fff",
                  }}
                  onValueChange={(itemValue, itemIndex) => setFrom(itemValue)}
                >
                  <Picker.Item label="00" value="00" color="white" />
                  <Picker.Item label="1:00" value="1" color="white" />
                  <Picker.Item label="2:00" value="2" color="white" />
                  <Picker.Item label="3:00" value="3" color="white" />
                  <Picker.Item label="4:00" value="4" color="white" />
                  <Picker.Item label="5:00" value="5" color="white" />
                  <Picker.Item label="6:00" value="6" color="white" />
                  <Picker.Item label="7:00" value="7" color="white" />
                  <Picker.Item label="8:00" value="8" color="white" />
                  <Picker.Item label="9:00" value="9" color="white" />
                  <Picker.Item label="10:00" value="10" color="white" />
                  <Picker.Item label="11:00" value="11" color="white" />
                  <Picker.Item label="12:00" value="12" color="white" />
                  <Picker.Item label="13:00" value="13" color="white" />
                  <Picker.Item label="14:00" value="14" color="white" />
                  <Picker.Item label="15:00" value="15" color="white" />
                  <Picker.Item label="16:00" value="16" color="white" />
                  <Picker.Item label="17:00" value="17" color="white" />
                  <Picker.Item label="18:00" value="18" color="white" />
                  <Picker.Item label="19:00" value="19" color="white" />
                  <Picker.Item label="20:00" value="20" color="white" />
                  <Picker.Item label="21:00" value="21" color="white" />
                  <Picker.Item label="22:00" value="22" color="white" />
                  <Picker.Item label="23:00" value="23" color="white" />
                  <Picker.Item label="24:00" value="24" color="white" />
                </Picker>
              </View>

              {from ? (
                <>
                  <View style={{ flex: 0.3, alignItems: "center" }}>
                    <Text
                      style={{
                        color: "#fff",
                        marginTop: 100,
                        fontWeight: "bold",
                        padding: 10,
                        borderRadius: 5,
                        fontSize: 22,
                      }}
                    >
                      till
                    </Text>
                  </View>

                  <View style={styles.button}>
                    <Picker
                      selectedValue={to}
                      style={{ height: 50, width: 100, color: "#fff" }}
                      onValueChange={(itemValue, itemIndex) => setTo(itemValue)}
                    >
                      <Picker.Item label="00" value="00" color="white" />
                      <Picker.Item label="1:00" value="1" color="white" />
                      <Picker.Item label="2:00" value="2" color="white" />
                      <Picker.Item label="3:00" value="3" color="white" />
                      <Picker.Item label="4:00" value="4" color="white" />
                      <Picker.Item label="5:00" value="5" color="white" />
                      <Picker.Item label="6:00" value="6" color="white" />
                      <Picker.Item label="7:00" value="7" color="white" />
                      <Picker.Item label="8:00" value="8" color="white" />
                      <Picker.Item label="9:00" value="9" color="white" />
                      <Picker.Item label="10:00" value="10" color="white" />
                      <Picker.Item label="11:00" value="11" color="white" />
                      <Picker.Item label="12:00" value="12" color="white" />
                      <Picker.Item label="13:00" value="13" color="white" />
                      <Picker.Item label="14:00" value="14" color="white" />
                      <Picker.Item label="15:00" value="15" color="white" />
                      <Picker.Item label="16:00" value="16" color="white" />
                      <Picker.Item label="17:00" value="17" color="white" />
                      <Picker.Item label="18:00" value="18" color="white" />
                      <Picker.Item label="19:00" value="19" color="white" />
                      <Picker.Item label="20:00" value="20" color="white" />
                      <Picker.Item label="21:00" value="21" color="white" />
                      <Picker.Item label="22:00" value="22" color="white" />
                      <Picker.Item label="23:00" value="23" color="white" />
                      <Picker.Item label="24:00" value="24" color="white" />
                    </Picker>
                  </View>
                </>
              ) : null}
            </View>
          ) : null}
          {to ? (
            <TouchableOpacity
              onPress={() => submitTime()}
              style={{
                borderBottomWidth: 1,
                borderBottomColor: "#fff",
                alignSelf: "center",
                position: "absolute",
                top: "60%",
              }}
            >
              <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 18 }}>
                Set
              </Text>
            </TouchableOpacity>
          ) : null}
        </View>
      ) : null}
    </View>
  );
};

export default workinghours;

workinghours.navigationOptions = () => {
  return {
    header: () => false,
  };
};

const styles = StyleSheet.create({
  inputBox: {
    width: "90%",
    height: 35,
    backgroundColor: "white",
    color: "#004FFE",
    fontSize: 15,
    borderWidth: 1,
    borderColor: "#004FFE",
    fontFamily: "DINNextMedium",
    borderRadius: 35,
    padding: 10,
  },
  profilePictureWrapper: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 1,
  },
  imagesGroup: {
    flexWrap: "wrap",
    margin: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    fontSize: 13,
    color: "#fff",
    fontFamily: "DINNextMedium",
    marginBottom: 10,
  },
  mainContainer: {
    flex: 1,
  },

  flatlistContainer: {
    flex: 10 / 9,
  },
  itemContainer: {
    justifyContent: "center",
    alignItems: "center",

    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
  },

  item_LogoContainer: {
    flex: 1 / 2,
    justifyContent: "center",
    alignItems: "center",
  },

  item_DescriptionContainer: {
    flex: 1 / 3,
    justifyContent: "center",
    alignItems: "center",

    marginTop: 5,
  },

  container: {
    padding: 22,
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#004FFE",
  },
  button: {
    flex: 0.3,
    width: "30%",
    height: 20,
    justifyContent: "center",
    alignItems: "center",
    borderColor: Platform.OS === "ios" ? "#fff" : "#fff",
    borderRadius: 5,
    borderWidth: Platform.OS === "ios" ? 0 : 1,
    marginRight: 15,
    backgroundColor: Platform.OS === "ios" ? "#004FFE" : "#004FFE",
    marginTop: Platform.OS === "ios" ? 0 : 100,
  },
  button2: {
    flex: 0.3,
    width: "30%",
    height: 60,
    justifyContent: "center",
    alignItems: "center",
    borderColor: Platform.OS === "ios" ? "#fff" : "#fff",
    borderRadius: 5,
    borderWidth: Platform.OS === "ios" ? 0 : 1,
    marginLeft: 15,
    backgroundColor: Platform.OS === "ios" ? "#004FFE" : "#004FFE",
    marginTop: Platform.OS === "ios" ? 0 : 100,
  },
});

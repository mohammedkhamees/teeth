import React, { useState, useEffect } from "react";
import {
  View,
  Modal,
  Image,
  TextInput,
  TouchableOpacity,
  Text,
  FlatList,
} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import profilepc from "../../../assets/images/profilepc.png";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Swipeout from "react-native-swipeout";
import styles from "./style";
import axios from "axios";

const Message = ({
  userId,
  doctorName,
  doctorImg,
  conversationId,
  userName,
  navigation,
  deleteMessage,
  email,
}) => {
  const swipSettings = [
    {
      onPress: () => {
        deleteMessage(conversationId);
      },
      text: "Delete",
      type: "delete",
    },
  ];

  return (
    <Swipeout style={{ marginBottom: 20 }} right={swipSettings}>
      <TouchableOpacity
        style={{
          height: 100,
          width: "100%",
          flexDirection: "row",
          alignItems: "center",
          backgroundColor: "#004FFE",
        }}
        onPress={() =>
          navigation.navigate("Chat", {
            userId,
            doctorName,
            doctorImg,
            conversationId,
            userName,
            email,
          })
        }
      >
        <View style={{ flex: 0.3 }}>
          <Image source={{ uri: doctorImg }} style={styles.profilePicture} />
        </View>

        <TouchableOpacity
          onPress={() =>
            navigation.navigate("Chat", {
              userId,
              doctorName,
              doctorImg,
              conversationId,
              userName,
              email,
            })
          }
          style={{ flex: 0.6, justifyContent: "center" }}
        >
          <Text style={{ fontSize: 20, color: "#fff", fontWeight: "bold" }}>
            {doctorName}
          </Text>
          <Text style={{ color: "#6164F0", fontWeight: "bold" }}>
            {doctorName}
          </Text>
        </TouchableOpacity>
      </TouchableOpacity>
    </Swipeout>
  );
};

const Massages = ({ navigation }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [messageId, setMessageId] = useState("");
  const [messages, setMessages] = useState([]);
  const [token, setToken] = useState("");
  const [role, setRole] = useState("");
  const [user, setUserName] = useState("");
  const [userEmail, setUserEmail] = useState("");

  useEffect(() => {
    getToken();
  }, []);

  const getToken = async () => {
    const token = JSON.parse(await AsyncStorage.getItem("token"));
    const userName = JSON.parse(await AsyncStorage.getItem("name"));
    const role = JSON.parse(await AsyncStorage.getItem("role"));
    const userEmail = JSON.parse(await AsyncStorage.getItem("email"));
    if (token) {
      setToken(token);
      setRole(role);
      setUserName(userName);
      setUserEmail(userEmail);
      getMessages(token);
    } else {
      console.log("ERROR GET TOKEN");
    }
  };

  const deleteMessage = (conversation_id) => {
    setMessages([
      ...messages.filter(
        (message) => message.conversation_id !== conversation_id
      ),
    ]);
    axios
      .post(
        `https://nextstageksa.com/clinic/api/RemoveConversations?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        { conversation_id },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        console.log("success");
      })
      .catch((error) => {
        console.log("deleteMessage ERROR : ", error);
      });
  };

  const getMessages = (token) => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/GetConversations?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        setMessages([...response.data]);
      })
      .catch((error) => {
        console.log("getMessages ERROR : ", error);
      });
  };

  return (
    <View style={{ flex: 1 }}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <TouchableOpacity
          style={styles.centeredView}
          onPressOut={() => {
            setModalVisible(false);
          }}
        >
          <View style={styles.modalView}>
            <TouchableOpacity
              style={[styles.button, styles.buttonClose]}
              onPress={() => {
                setModalVisible(false);
                deleteMessage(messageId);
              }}
            >
              <Text style={styles.textStyle}> Delete </Text>
            </TouchableOpacity>
            <View style={{ height: 10 }} />
            <View style={{ borderBottomWidth: 1, width: "90%" }} />
            <View style={{ height: 10 }} />
            <TouchableOpacity
              style={[styles.button, styles.buttonClose]}
              onPress={() => {
                setModalVisible(false);
                navigation.navigate("report");
              }}
            >
              <Text style={styles.textStyle}> Report </Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </Modal>

      <View
        style={{
          justifyContent: "center",
          backgroundColor: "#004FFE",
          height: "12%",
          borderBottomEndRadius: 20,
          borderBottomLeftRadius: 20,
        }}
      >
        <View
          style={{
            marginTop: 20,
            marginRight: 20,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ flex: 0.1 }}
          >
            <FontAwesome5 active size={30} name={"angle-left"} color="white" />
          </TouchableOpacity>

          <View style={{ flex: 0.8, alignItems: "center" }}>
            <Text style={{ color: "#fff", fontSize: 20 }}> Massages</Text>
          </View>
        </View>
      </View>

      <View style={{ height: 15, width: "100%" }} />

      <View style={{ flex: 10 / 9 }}>
        <FlatList
          data={messages}
          renderItem={({ item }) => (
            <Message
              doctorName={item.user_name}
              userId={item.user_id}
              doctorImg={item.profile_image}
              conversationId={item.conversation_id}
              userName={user}
              email={userEmail}
              deleteMessage={deleteMessage}
              navigation={navigation}
            />
          )}
          keyExtractor={(item) => item.conversation_id}
        />
      </View>
    </View>
  );
};

export default Massages;

Massages.navigationOptions = () => {
  return {
    header: () => false,
  };
};

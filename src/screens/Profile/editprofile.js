import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Image,
  ActivityIndicator,
} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import profilepc from "../../../assets/images/profilepc.png";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import * as ImagePicker from "expo-image-picker";

const { width, height } = Dimensions.get("window");

const EditProfile = ({ navigation }) => {
  const [profile_image, setProfile_image] = useState("");
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [age, setAge] = useState("");
  const [gender, setGender] = useState("");
  const [email, setEmail] = useState("");
  const [token, setToken] = useState("");
  const [uri, setUri] = useState("");

  useEffect(() => {
    getToken();
  }, []);

  const getToken = async () => {
    const token = JSON.parse(await AsyncStorage.getItem("token"));
    const name = JSON.parse(await AsyncStorage.getItem("name"));
    const image = JSON.parse(await AsyncStorage.getItem("profile_image"));
    const phone = JSON.parse(await AsyncStorage.getItem("phone"));
    const age = JSON.parse(await AsyncStorage.getItem("Age"));
    const gender = JSON.parse(await AsyncStorage.getItem("Gender"));
    const email = JSON.parse(await AsyncStorage.getItem("email"));

    if (token) {
      setName(name);
      setProfile_image(image);
      setPhone(phone);
      setAge(age);
      setGender(gender);
      setEmail(email);
      setToken(token);
      setUri(image);
    } else {
      console.log("ERROR GET TOKEN");
    }
  };

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [16, 9],
      quality: 1,
      base64: true,
    });

    if (!result.cancelled) {
      setProfile_image(result.base64);
      setUri(`data:image/png;base64,${result.base64}`);
    }
  };

  const editInfo = () => {
    axios
      .post(
        "https://nextstageksa.com/clinic/api/edit_profile?key=6Q4AE52F15F8B82735E9485CA3EC7",
        { profile_image, name, phone },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then(async (res) => {
        console.log(res.data);
        await AsyncStorage.setItem(
          "name",
          JSON.stringify(res.data.message.name)
        );
        await AsyncStorage.setItem(
          "phone",
          JSON.stringify(res.data.message.phone)
        );
        await AsyncStorage.setItem(
          "profile_image",
          JSON.stringify(res.data.message.profile_image)
        );
        navigation.navigate("Home");
      })
      .catch((err) => console.log("err", err));
  };

  return (
    <View style={{ flex: 1 }}>
      <View
        style={{
          paddingTop: 20,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "#004FFE",
          height: "12%",
          borderBottomEndRadius: 20,
          borderBottomLeftRadius: 20,
          width: "100%",
        }}
      >
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <FontAwesome5 active size={30} name={"angle-left"} color="white" />
        </TouchableOpacity>

        <View style={{ flex: 0.8, alignItems: "center" }}>
          <Text style={{ color: "#fff", fontSize: 20 }}>Edit Profile</Text>
        </View>
      </View>

      <View
        style={{
          flex: 1,
          marginTop: 25,
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "#fff",
          width: "100%",
        }}
      >
        <View style={styles.profilePictureWrapper}>
          <Image
            source={uri ? { uri: uri } : profilepc}
            style={styles.profilePicture}
            resizeMode="contain"
            PlaceholderContent={<ActivityIndicator />}
          />

          <TouchableOpacity
            style={{
              marginTop: -40,
            }}
            onPress={pickImage}
          >
            <Text style={styles.text}>Upload</Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            flex: 1,
            backgroundColor: "#fff",
            width: "100%",
            height: "100%",
            alignItems: "center",
          }}
        >
          <TextInput
            style={styles.inputBox}
            autoCorrect={false}
            placeholder={"Name"}
            placeholderTextColor="#305FDF"
            selectionColor="#305FDF"
            keyboardType="email-address"
            enablesReturnKeyAutomatically={true}
            defaultValue={name}
            onChangeText={setName}
          />

          <TextInput
            style={styles.inputBox}
            autoCorrect={false}
            placeholder={"Phone Number"}
            placeholderTextColor="#305FDF"
            selectionColor="#305FDF"
            keyboardType="email-address"
            enablesReturnKeyAutomatically={true}
            defaultValue={phone}
            onChangeText={setPhone}
          />

          <TextInput
            style={styles.inputBoxDisable}
            autoCorrect={false}
            placeholder={"Email"}
            placeholderTextColor="#aaa"
            selectionColor="#305FDF"
            keyboardType="email-address"
            enablesReturnKeyAutomatically={true}
            value={email}
            editable={false}
          />

          <TextInput
            style={styles.inputBoxDisable}
            autoCorrect={false}
            placeholder={"Password"}
            placeholderTextColor="#305FDF"
            selectionColor="#305FDF"
            keyboardType="email-address"
            enablesReturnKeyAutomatically={true}
            value="mohammedasdasdasdasd"
            secureTextEntry={true}
            editable={false}
          />

          <TextInput
            style={styles.inputBoxDisable}
            autoCorrect={false}
            placeholder={"Confirm Password"}
            placeholderTextColor="#305FDF"
            selectionColor="#305FDF"
            keyboardType="email-address"
            enablesReturnKeyAutomatically={true}
            value="mohammedasdasdasdasda"
            secureTextEntry={true}
            editable={false}
          />

          <TextInput
            style={styles.inputBoxDisable}
            autoCorrect={false}
            placeholder={"Gender"}
            placeholderTextColor="#aaa"
            selectionColor="#305FDF"
            keyboardType="email-address"
            enablesReturnKeyAutomatically={true}
            value={gender}
            editable={false}
          />

          <TextInput
            style={styles.inputBoxDisable}
            autoCorrect={false}
            placeholder={"Age"}
            placeholderTextColor="#aaa"
            selectionColor="#305FDF"
            keyboardType="email-address"
            enablesReturnKeyAutomatically={true}
            value={age}
            editable={false}
          />
          <TouchableOpacity
            onPress={editInfo}
            style={{
              width: "70%",
              backgroundColor: "#305FDF",
              padding: 10,
              borderWidth: 2,
              borderRadius: 28,
              borderColor: "white",
              justifyContent: "center",
              alignItems: "center",
              marginTop: "10%",
            }}
          >
            <Text style={{ color: "#fff", fontSize: 25, fontWeight: "bold" }}>
              Save
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default EditProfile;

EditProfile.navigationOptions = () => {
  return {
    header: () => false,
  };
};

const styles = StyleSheet.create({
  inputBox: {
    width: "70%",
    height: 50,
    borderBottomColor: "#6B87D6",
    borderBottomWidth: 1,
    color: "blue",
    fontSize: 15,
    borderColor: "red",
  },
  inputBoxDisable: {
    width: "70%",
    height: 50,
    borderBottomColor: "#6B87D6",
    borderBottomWidth: 1,
    color: "#ccc",
    fontSize: 15,
    borderColor: "red",
  },
  profilePictureWrapper: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
  },
  profilePicture: {
    width: height / 4.7,
    height: height / 4.7,
    resizeMode: "cover",
    borderRadius: height / 6,
  },
  imagesGroup: {
    flexWrap: "wrap",
    margin: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    fontSize: 13,
    color: "#ccc",
    marginBottom: 10,
  },
});

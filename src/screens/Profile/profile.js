import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Modal,
  Dimensions,
  Image,
  FlatList,
  ActivityIndicator,
  Alert,
} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { AirbnbRating, colors, Divider } from "react-native-elements";
import { StatusBar } from "expo-status-bar";
import profilepc from "../../../assets/images/profilepc.png";
import { AsyncStorage } from "react-native";
import axios from "axios";

const { width, height } = Dimensions.get("window");

const Days = ({ day, times, getDayTimes }) => {
  if (times.length) {
    return (
      <View>
        <View style={{ padding: 8 }}>
          <TouchableOpacity
            style={{
              padding: 6,
              borderRadius: 30,
              backgroundColor: "#004FFE",
            }}
            onPress={() => getDayTimes(day)}
          >
            <Text style={{ color: "#fff", textAlign: "center" }}>{day}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  } else {
    return (
      <View>
        <View style={{ padding: 8 }}>
          <View
            style={{
              padding: 6,
              borderRadius: 30,
              backgroundColor: "#CECED4",
            }}
          >
            <Text style={{ color: "#fff", textAlign: "center" }}>{day}</Text>
          </View>

          <View style={styles.item_DescriptionContainer}></View>
        </View>
      </View>
    );
  }
};

const Profile = ({ navigation }) => {
  const drItem = navigation.getParam("drItem");
  const name = navigation.getParam("name");
  const image = navigation.getParam("image");
  
  const [token, setToken] = useState("");
  const [role, setRole] = useState(0);
  const [bookcomponent, setBookcomponent] = useState(false);
  const [days, setDays] = useState([]);
  const [day, setDay] = useState("");
  const [time, setTime] = useState([]);

  useEffect(() => {
    getToken();
  }, []);

  const getToken = async () => {
    const token = JSON.parse(await AsyncStorage.getItem("token"));
    const role = JSON.parse(await AsyncStorage.getItem("role"));
    if (token) {
      setToken(token);
      setRole(role);
      getDays(token);
    } else {
      console.log("ERROR GET TOKEN");
    }
  };

  const getDays = () => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/GetSavenDate?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        {
          headers: {
            Authorization: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvbmV4dHN0YWdla3NhLmNvbVwvY2xpbmljXC9hcGlcL2xvZ2luIiwiaWF0IjoxNjI4MDg4NjE2LCJleHAiOjE2MjgyNjE0MTYsIm5iZiI6MTYyODA4ODYxNiwianRpIjoicFBwbVBhdXdsZjY5TU9ZcSIsInN1YiI6MzgsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.BdviPPQTnWQfXwEM11ggACbCWHz1Wzfdyhy5IO5iGDY`,
          },
        }
      )
      .then((response) => {
        setDays([...response.data]);
      })
      .catch((error) => {
        console.log("getTime ERROR : ", error);
      });
  };

  const getDayTimes = (dayName) => {
    const dayTimes = days.find((day) => day.day === dayName);
    setTime([...dayTimes.times]);
  };

  const addAppointment = (date, time_id, dr_id) => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/Appointments/new?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        { date: "2021-08-21", time_id, dr_id },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        Alert.alert("Added to your pending Reservations");
      })
      .catch((error) => {
        console.log("getTime ERROR : ", error);
      });
  };

  const addChat = () => {
    axios
      .post(
        `https://nextstageksa.com/clinic/api/CreateConversations?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        { dr_id: drItem.id },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        navigation.navigate("Massages");
      })
      .catch((error) => {
        console.log("getTime ERROR : ", error);
      });
  };

  return (
    <View style={{ flex: 1 }}>
      <View style={{ alignItems: "center", justifyContent: "center" }}>
        <View
          style={{
            width: "100%",
            height: "50%",
            backgroundColor: "#004FFE",
            alignItems: "center",
          }}
        >
          <View style={{ height: "15%" }} />

          <View
            style={{
              shadowRadius: 14.78,
              shadowOpacity: 0.55,
              elevation: 10,
              flexDirection: "row",
              height: "25%",
              justifyContent: "center",
              backgroundColor: "#004FFE",
              width: "100%",
              alignItems: "center",
              borderBottomEndRadius: 20,
              borderBottomLeftRadius: 20,
              paddingTop: 10,
            }}
          >
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={{ flex: 0.2, marginTop: "10%" }}
            >
              <FontAwesome5
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  alignSelf: "center",
                }}
                active
                size={30}
                name={"angle-left"}
                color="white"
              />
            </TouchableOpacity>

            <View style={{ flex: 0.6, marginTop: "10%" }}>
              <Text
                style={{
                  flex: 0.4,
                  justifyContent: "center",
                  alignItems: "center",
                  alignSelf: "center",
                  color: "#fff",
                  fontSize: 23,
                }}
              >
                Details
              </Text>
            </View>

            <View style={{ flex: 0.2, backgroundColor: "blue" }}></View>
          </View>

          <View style={{ flexDirection: "row", padding: 10, marginTop: 10 }}>
            <TouchableOpacity style={{ flex: 0.5, alignItems: "center" }}>
              <Image
                source={{ uri: image }}
                style={styles.profilePicture}
                resizeMode="contain"
                PlaceholderContent={<ActivityIndicator />}
              />
            </TouchableOpacity>

            <View style={{ padding: 10, flex: 0.6 }}>
              <Text style={{ marginTop: 12, fontSize: 20, color: "#CFD0D5" }}>
                {name}
              </Text>
              <Text style={{ marginTop: 12, fontSize: 20, color: "#CFD0D5" }}>
                4.5
              </Text>
              <Text
                style={{
                  marginTop: 14,
                  fontSize: 15,
                  color: "#CFD0D5",
                  borderColor: "#fff",
                }}
              >
                Location
              </Text>
            </View>
          </View>

          <View
            style={{
              width: "100%",
              justifyContent: "center",
              alignContent: "center",
            }}
          >
            <TouchableOpacity
              onPress={addChat}
              style={{
                flexDirection: "row",
                borderTopWidth: 1,
                justifyContent: "center",
                alignContent: "center",
                borderColor: "#fff",
                borderBottomWidth: 1,
                justifyContent: "center",
                alignContent: "center",
                borderBottomWidth: 1,
              }}
            >
              <View style={{ flex: 0.9 }}>
                <Text style={{ padding: 15, marginRight: 20, color: "#fff" }}>
                  Massage
                </Text>
              </View>

              <View style={{ flex: 0.1, alignSelf: "center" }}>
                <FontAwesome5
                  active
                  size={30}
                  name={"angle-right"}
                  color="white"
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>

        <View style={{ backgroundColor: "#fff", height: "70%", width: "100%" }}>
          <View
            style={{
              alignSelf: "center",
              marginTop: "8%",
              borderBottomWidth: 1,
              borderColor: "#004FFE",
              height: 1,
              width: "93%",
            }}
          />

          <View style={{ padding: 12 }}>
            <Text style={{ padding: 5, fontSize: 15, color: "#004FFE" }}>
              {/* Specialized On: {drItem.specialized} */}
            </Text>
            <Text style={{ padding: 5, fontSize: 15, color: "#004FFE" }}>
              {/* Resident At: {drItem.resident} */}
            </Text>
            {bookcomponent ? null : (
              <Text style={{ padding: 5, fontSize: 15, color: "#004FFE" }}>
                Insurance Companies:
              </Text>
            )}
          </View>

          <View
            style={{
              alignSelf: "center",
              marginTop: "6%",
              height: 1,
              width: "93%",
            }}
          />

          {!bookcomponent && role === 4 ? (
            <View>
              <TouchableOpacity
                onPress={() => setBookcomponent(true)}
                style={{
                  width: "40%",
                  alignSelf: "center",
                  backgroundColor: "#004FFE",
                  padding: 13,
                  borderWidth: 2,
                  borderRadius: 28,
                  borderColor: "white",
                }}
              >
                <Text
                  style={{
                    alignSelf: "center",
                    color: "#fff",
                    fontSize: 15,
                    fontWeight: "bold",
                  }}
                >
                  Reservate
                </Text>
              </TouchableOpacity>

              <View
                style={{
                  marginTop: "5%",
                  alignSelf: "center",
                  width: "93%",
                  paddingTop: "8%",
                  borderTopWidth: 1,
                  borderColor: "#004FFE",
                }}
              >
                <AirbnbRating
                  showRating={false}
                  count={5}
                  defaultRating={1 != 0 ? 4 : 0}
                  //onFinishRating={this.ratingCompleted}
                  isDisabled={true}
                  size={23}
                  //selectedColor="#83c246"
                  reviewColor="#f1c40f"
                />
              </View>

              <View
                style={{
                  alignSelf: "center",
                  marginTop: "7%",
                  borderBottomWidth: 1,
                  borderColor: "#004FFE",
                }}
              >
                <Text style={{ color: "#004FFE" }}> Add a review </Text>
              </View>
            </View>
          ) : (
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
              }}
            >
              <FlatList
                data={days}
                horizontal={true}
                renderItem={({ item }) => (
                  <Days
                    day={item.day}
                    times={item.times}
                    getDayTimes={getDayTimes}
                  />
                )}
                keyExtractor={(item) => item.day}
              />
            </View>
          )}
          {time.length ? (
            <View style={{ padding: 30, backgroundColor: "#004FFE" }}>
              <FlatList
                data={time}
                horizontal={false}
                renderItem={({ item }) => (
                  <View
                    style={{
                      flexDirection: "row",
                      borderTopColor: "#fff",
                      borderTopWidth: 1,
                      padding: 5,
                      alignItems: "center",
                      justifyContent: "space-between",
                    }}
                  >
                    <Text style={{ color: "#fff" }}>{item.display_time}</Text>
                    <TouchableOpacity
                      onPress={() =>
                        addAppointment(item.display_time, item.id, drItem.id)
                      }
                    >
                      <Text
                        style={{
                          color: "#fff",
                          borderBottomWidth: 1,
                          borderBottomColor: "#fff",
                        }}
                      >
                        Book
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
                keyExtractor={(item) => item.id}
              />
              <View style={{ height: 50 }} />
              <TouchableOpacity
                onPress={() => navigation.navigate("ReservationsPending")}
                style={{
                  backgroundColor: "#DEE8FE",
                  width: "50%",
                  alignSelf: "center",
                  borderRadius: 33,
                  height: 30,
                  justifyContent: "center",
                }}
              >
                <Text style={{ alignSelf: "center" }}> Done </Text>
              </TouchableOpacity>
            </View>
          ) : null}
        </View>
      </View>
    </View>
  );
};

export default Profile;

Profile.navigationOptions = () => {
  return {
    header: () => false,
  };
};

const styles = StyleSheet.create({
  inputBox: {
    width: "90%",
    height: 35,
    borderBottomColor: "#6B87D6",
    backgroundColor: "#fff",
    color: "#004FFE",
    fontSize: 15,
    borderRadius: 35,
  },
  profilePictureWrapper: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 1,
  },
  profilePicture: {
    width: height / 5.3,
    height: height / 5.3,
    resizeMode: "cover",
    borderRadius: height / 6,
  },
  profilePictureDoctors: {
    width: height / 8,
    height: height / 8,
    resizeMode: "cover",
    borderRadius: height / 8,
    padding: 1,
  },
  imagesGroup: {
    flexWrap: "wrap",
    margin: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    fontSize: 13,
    color: "#fff",
    marginBottom: 10,
  },
});

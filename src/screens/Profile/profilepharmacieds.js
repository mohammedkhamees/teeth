import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  FlatList,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Image,
  ActivityIndicator,
  SafeAreaView,
} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { AirbnbRating, colors, Divider } from "react-native-elements";
import profilepc from "../../../assets/images/profilepc.png";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";

const { width, height } = Dimensions.get("window");

const Producto = ({ equipo, nombre, price, image, id, token, role }) => {
  const [quantity_ordered, setQuantity_ordered] = useState(0);

  const addOrder = () => {
    axios
      .post(
        `https://nextstageksa.com/clinic/api/order/addOrder/${id}?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        { quantity_ordered },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        console.log(response.data.message);
        setQuantity_ordered(0);
      })
      .catch((error) => {
        console.log("addOrder error", error);
      });
  };

  return (
    <View style={styles.mainContainer}>
      <View style={styles.itemContainer}>
        <ImageBackground
          source={
            image
              ? { uri: `https://nextstageksa.com/clinic/${image}` }
              : profilepc
          }
          style={styles.profilePictureMed}
        >
          {role === 4 && (
            <View
              style={{
                flexDirection: "row",
                padding: 10,
                marginTop: "60%",
                alignSelf: "center",
                width: "100%",
                alignItems: "center",
                justifyContent: "center",
                height: "40%",
                backgroundColor: "transparent",
              }}
            >
              <TouchableOpacity
                onPress={() => setQuantity_ordered(quantity_ordered + 1)}
              >
                <FontAwesome5 active size={15} name={"plus"} color="#004FFE" />
              </TouchableOpacity>

              <View>
                {quantity_ordered > 0 ? (
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "#004FFE",
                      fontSize: 15,
                    }}
                  >
                    {" "}
                    {quantity_ordered}{" "}
                  </Text>
                ) : (
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "#004FFE",
                      fontSize: 15,
                    }}
                  >
                    {" "}
                    00{" "}
                  </Text>
                )}
              </View>

              <TouchableOpacity
                onPress={() =>
                  quantity_ordered > 0
                    ? setQuantity_ordered(quantity_ordered - 1)
                    : null
                }
              >
                <FontAwesome5 active size={15} name={"minus"} color="#004FFE" />
              </TouchableOpacity>
            </View>
          )}
        </ImageBackground>

        <View style={{ marginTop: 20 }}>
          <Text style={styles.textName}>Med Name: {equipo}</Text>
          <Text style={styles.textTeam}>QTY: {nombre}</Text>
          <Text style={styles.textTeam}>Price: {price} JOD</Text>
        </View>
      </View>

      {role === 4 && (
        <TouchableOpacity
          onPress={addOrder}
          style={{
            width: "40%",
            alignSelf: "center",
            backgroundColor: "#004FFE",
            padding: 13,
            borderWidth: 2,
            borderRadius: 28,
            borderColor: "white",
            marginTop: 10,
          }}
        >
          <Text
            style={{
              alignSelf: "center",
              color: "#fff",
              fontSize: 15,
              fontWeight: "bold",
            }}
          >
            Order
          </Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

const profilepharmacieds = ({ navigation }) => {
  const [medicine, setMedicine] = useState([]);
  const [token, setToken] = useState("");
  const [role, setRole] = useState("");
  const [pharName, setPharName] = useState("");

  const key_phar = navigation.getParam("key_phar");
  const name = navigation.getParam("name");
  const cityId = navigation.getParam("cityId");

  useEffect(() => {
    setPharName(name);
  });

  useEffect(() => {
    getToken();
  }, [pharName]);

  const getToken = async () => {
    const token = JSON.parse(await AsyncStorage.getItem("token"));
    const role = JSON.parse(await AsyncStorage.getItem("role"));
    if (token) {
      setToken(token);
      setRole(role);
      getMedicine(token);
    } else {
      console.log("ERROR GET TOKEN");
    }
  };

  const getMedicine = (token) => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/Medicine?key=6Q4AE52F15F8B82735E9485CA3EC7&key_phar=${key_phar}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        setMedicine(response.data.Data.medicine);
      })
      .catch((error) => {
        console.log("getMedicine error", error);
      });
  };

  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#fff",
      }}
    >
      <View
        style={{
          shadowRadius: 14.78,
          shadowOpacity: 0.55,
          backgroundColor: "#004FFE",
          width: "100%",
          borderBottomEndRadius: 20,
          borderBottomLeftRadius: 20,
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          position: "absolute",
          top: 0,
          left: 0,
          height: "12%",
          zIndex: 1,
        }}
      >
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{
            position: "absolute",
            top: "50%",
            left: "7%",
          }}
        >
          <FontAwesome5 active size={30} name={"angle-left"} color="white" />
        </TouchableOpacity>

        <Text
          style={{
            color: "#fff",
            fontSize: 23,
            textAlign: "center",
            paddingTop: "7%",
          }}
        >
          Details
        </Text>
      </View>

      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          width: "100%",
          paddingTop: "25%",
          paddingLeft: "5%",
          backgroundColor: "#004FFE",
        }}
      >
        <TouchableOpacity>
          <ImageBackground
            source={profilepc}
            style={styles.profilePicture}
            resizeMode="contain"
            PlaceholderContent={<ActivityIndicator />}
          />
        </TouchableOpacity>

        <View
          onPress={() => navigation.navigate("pharmacies")}
          style={{
            marginLeft: "10%",
            alignItems: "center",
          }}
        >
          <Text style={{ fontSize: 20, color: "#fff" }}>{name}</Text>
          <Text
            style={{
              paddingTop: 5,
              fontSize: 20,
              color: "#fff",
              alignItems: "center",
            }}
          >
            4.5
          </Text>

          <Text
            style={{
              paddingTop: 12,
              fontSize: 15,
              color: "#fff",
              borderColor: "#fff",
            }}
          >
            {cityId}
          </Text>
        </View>
      </View>

      <View style={{ height: "62%", width: "100%" }}>
        <View
          style={{
            alignItems: "center",
            width: "100%",
            backgroundColor: "#004FFE",
            padding: 11,
          }}
        >
          <TextInput
            style={styles.inputBox}
            autoCorrect={false}
            placeholder={"  Search"}
            placeholderTextColor="#768AE1"
            selectionColor="#004FFE"
            keyboardType="email-address"
            enablesReturnKeyAutomatically={true}
          />
        </View>
        <View style={styles.mainContainer}>
          <View style={styles.flatlistContainer}>
            <FlatList
              data={medicine}
              numColumns={2}
              renderItem={({ item }) => (
                <Producto
                  equipo={item.name}
                  nombre={item.quantity}
                  price={item.price}
                  image={item.image}
                  id={item.id}
                  token={token}
                  role={role}
                />
              )}
              keyExtractor={(item) => item.id}
            />
          </View>

          <View style={{ marginTop: "5%", alignSelf: "center", width: "100%" }}>
            <AirbnbRating
              showRating={false}
              count={5}
              defaultRating={1 != 0 ? 4 : 0}
              isDisabled={true}
              size={23}
              reviewColor="#f1c40f"
            />
          </View>

          <View
            style={{
              alignSelf: "center",
              borderBottomWidth: 1,
              borderColor: "#004FFE",
            }}
          >
            <Text
              style={{
                color: "#004FFE",
                borderBottomWidth: 1,
                marginTop: "5%",
              }}
            >
              {" "}
              Add a review{" "}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default profilepharmacieds;

profilepharmacieds.navigationOptions = () => {
  return {
    header: () => false,
  };
};

const styles = StyleSheet.create({
  inputBox: {
    width: "90%",
    height: 35,
    borderBottomColor: "#6B87D6",
    backgroundColor: "#fff",
    color: "#004FFE",
    fontSize: 15,
    borderRadius: 35,
    padding: 10,
  },
  profilePictureWrapper: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 1,
  },
  profilePicture: {
    width: height / 5.3,
    height: height / 5.3,
    resizeMode: "cover",
    borderRadius: height / 6,
  },
  profilePictureMed: {
    width: height / 7,
    height: height / 7,
    borderRadius: height / 10,
  },
  profilePictureDoctors: {
    width: height / 6,
    height: height / 6,
    resizeMode: "cover",
    borderRadius: height / 6,
    padding: 1,
  },
  imagesGroup: {
    flexWrap: "wrap",
    margin: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    fontSize: 13,
    color: "#fff",
    marginBottom: 10,
  },
  mainContainer: {
    flex: 1,
    marginTop: 10,
  },

  flatlistContainer: {
    flex: 10 / 9,
  },
  itemContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
  },
  textPM: {
    fontSize: 22,
    borderWidth: 2,
    borderRadius: 50,
  },
});

import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Alert,
} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";

const { width, height } = Dimensions.get("window");

const AddSaved = ({ navigation }) => {
  const [token, setToken] = useState("");
  const [folder_name, setFolder_name] = useState("");

  const doctor_id = navigation.getParam("doctor_id");

  useEffect(() => {
    getToken();
  }, []);

  const getToken = async () => {
    const token = JSON.parse(await AsyncStorage.getItem("token"));
    if (token) {
      setToken(token);
    } else {
      console.log("ERROR GET TOKEN");
    }
  };

  const addFolders = () => {
    if (doctor_id) {
      axios
        .post(
          `https://nextstageksa.com/clinic/api/favorite/newFolder`,
          { folder_name },
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        )
        .then((response) => {
          const id = response.data.data.id;
          axios
            .post(
              `https://nextstageksa.com/clinic/api/favorite/doctor/${doctor_id}/folder/${id}`,
              {
                headers: {
                  Authorization: `Bearer ${token}`,
                },
              }
            )
            .then((response) => {
              Alert.alert("Added to New Collection");
              navigation.navigate("SavedFolders");
            })
            .catch((error) => {
              console.log("addFoldersfromHome error", error);
            });
        })
        .catch((error) => {
          console.log("error", error);
        });
    } else {
      axios
        .post(
          `https://nextstageksa.com/clinic/api/favorite/newFolder`,
          { folder_name },
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        )
        .then((response) => {
          Alert.alert(
            "Add Collection",
            "You Created a Collection Succussfully"
          );
          navigation.navigate("SavedFolders");
        })
        .catch((error) => {
          console.log("error", error);
        });
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "#004FFE",
          height: "12%",
          borderBottomEndRadius: 20,
          borderBottomLeftRadius: 20,
          paddingTop: "10%",
        }}
      >
        <TouchableOpacity
          style={{ position: "absolute", left: "10%", top: "90%" }}
          onPress={() => navigation.goBack()}
        >
          <FontAwesome5 active size={30} name={"angle-left"} color="white" />
        </TouchableOpacity>
        <Text style={{ color: "#fff", fontSize: 23 }}>Saved</Text>
      </View>
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          marginBottom: "10%",
        }}
      >
        <TextInput
          style={styles.inputBox}
          autoCorrect={false}
          placeholder={"Category Title..."}
          placeholderTextColor="#768AE1"
          selectionColor="#004FFE"
          keyboardType="email-address"
          enablesReturnKeyAutomatically={true}
          onChangeText={setFolder_name}
        />

        <TouchableOpacity
          onPress={addFolders}
          style={{
            width: "70%",
            alignItems: "center",
            backgroundColor: "#004FFE",
            padding: 10,
            borderWidth: 2,
            borderRadius: 28,
            borderColor: "white",
            marginTop: "10%",
          }}
        >
          <Text style={{ color: "#fff", fontSize: 25, fontWeight: "bold" }}>
            Add
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default AddSaved;

AddSaved.navigationOptions = () => {
  return {
    header: () => false,
  };
};

const styles = StyleSheet.create({
  inputBox: {
    width: 250,
    height: 50,
    borderBottomColor: "#6B87D6",
    borderBottomWidth: 1,
    color: "#004FFE",
    fontSize: 15,
    borderColor: "red",
  },
  profilePictureWrapper: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 50,
  },
  profilePicture: {
    width: height / 3.7,
    height: height / 3.7,
    resizeMode: "cover",
    borderRadius: height / 6,
  },
  imagesGroup: {
    flexWrap: "wrap",
    margin: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    fontSize: 13,
    color: "#fff",
    marginBottom: 10,
  },
});

import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Image,
  ActivityIndicator,
  FlatList,
} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";

const { width, height } = Dimensions.get("window");

const Folder = ({ title, navigation, deleteFolder, id, token }) => {
  return (
    <View style={{ padding: 20, justifyContent: "center", marginTop: 10 }}>
      <View
        style={{
          borderWidth: 1,
          borderColor: "#004FFE",
          width: 150,
          height: 150,
          borderRadius: 30,
        }}
      >
        <View
          style={{
            alignItems: "center",
            marginTop: "5%",
            marginLift: "11%",
          }}
        >
          <TouchableOpacity onPress={() => deleteFolder(id)}>
            <FontAwesome5
              style={{ marginLeft: "70%" }}
              active
              size={20}
              name={"trash-alt"}
              color="#004FFE"
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("SavedList", {
                id,
                title,
                token,
              });
            }}
          >
            <Text style={{ marginTop: "65%", fontWeight: "bold" }}>
              {title}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const SavedFolders = ({ navigation }) => {
  const [folders, setFolders] = useState([]);
  const [filterdFolders, setFilterdFolders] = useState([]);
  const [token, setToken] = useState("");

  useEffect(() => {
    getToken();
  }, []);

  const getToken = async () => {
    const token = JSON.parse(await AsyncStorage.getItem("token"));
    if (token) {
      setToken(token);
      getFolders(token);
    } else {
      console.log("ERROR GET TOKEN");
    }
  };

  const getFolders = (token) => {
    axios
      .get(`https://nextstageksa.com/clinic/api/favorite`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        setFolders([...response.data.data]);
      })
      .catch((error) => {
        console.log("error", error);
      });
  };

  const deleteFolder = (id) => {
    axios
      .delete(`https://nextstageksa.com/clinic/api/favorite/folder/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        setFolders([...folders.filter((folder) => folder.id !== id)]);
      })
      .catch((error) => {
        console.log("error", error);
      });
  };

  const searchFolder = (name) => {
    setFilterdFolders([
      ...folders.filter((folder) => folder.folder_name.includes(name)),
    ]);
  };

  return (
    <View style={{ flex: 1 }}>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-around",
          backgroundColor: "#004FFE",
          height: "12%",
          borderBottomEndRadius: 20,
          borderBottomLeftRadius: 20,
          paddingTop: "10%",
        }}
      >
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <FontAwesome5 active size={30} name={"angle-left"} color="white" />
        </TouchableOpacity>
        <Text style={{ color: "#fff", fontSize: 23 }}>Saved</Text>
        <TouchableOpacity onPress={() => navigation.navigate("AddSaved")}>
          <FontAwesome5 active size={25} name={"plus"} color="white" />
        </TouchableOpacity>
      </View>
      <View
        style={{
          width: "90%",
          alignItems: "center",
          alignSelf: "center",
          justifyContent: "center",
          flexDirection: "row",
          marginTop: "10%",
        }}
      >
        <View style={{ alignItems: "center", alignItems: "center" }}>
          <TouchableOpacity onPress={() => console.log(true)}>
            <FontAwesome5 active size={25} name={"filter"} color="#004FFE" />
          </TouchableOpacity>
        </View>

        <View style={{ flex: 0.9, alignItems: "center" }}>
          <TextInput
            style={styles.inputBox}
            autoCorrect={false}
            placeholder={"  Search"}
            placeholderTextColor="#768AE1"
            selectionColor="#004FFE"
            keyboardType="email-address"
            enablesReturnKeyAutomatically={true}
            onChangeText={(name) => searchFolder(name)}
          />
        </View>
      </View>

      <View style={styles.mainContainer}>
        <View style={styles.flatlistContainer}>
          <FlatList
            data={filterdFolders.length ? filterdFolders : folders}
            numColumns={2}
            renderItem={({ item }) => (
              <Folder
                title={item.folder_name}
                navigation={navigation}
                deleteFolder={deleteFolder}
                id={item.id}
                token={token}
              />
            )}
            keyExtractor={(item) => item.id}
          />
        </View>
      </View>
    </View>
  );
};

export default SavedFolders;

SavedFolders.navigationOptions = () => {
  return {
    header: () => false,
  };
};

const styles = StyleSheet.create({
  inputBox: {
    padding: 10,
    width: "90%",
    height: 35,
    backgroundColor: "#fff",
    color: "#004FFE",
    fontSize: 15,
    borderRadius: 35,
    borderWidth: 1,
    borderColor: "#004FFE",
  },

  profilePictureWrapper: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 50,
  },
  profilePicture: {
    width: height / 3.7,
    height: height / 3.7,
    resizeMode: "cover",
    borderRadius: height / 6,
  },
  imagesGroup: {
    flexWrap: "wrap",
    margin: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    fontSize: 13,
    color: "#fff",
    marginBottom: 10,
  },
  mainContainer: {
    flex: 1,
  },

  flatlistContainer: {
    flex: 10 / 9,
    alignSelf: "center",
  },
});

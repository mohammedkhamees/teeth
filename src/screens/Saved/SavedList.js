import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  Modal,
  SafeAreaView,
  Image,
  TextInput,
  TouchableOpacity,
  Text,
  FlatList,
  Dimensions,
} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { StatusBar } from "expo-status-bar";
import profilepc from "../../../assets/images/profilepc.png";
import axios from "axios";

const Docotor = ({ doctor_id, doctor_name, doctor_image }) => {
  return (
    <View>
      <View
        style={{
          height: 100,
          width: "100%",
          flexDirection: "row",
          alignItems: "center",
          backgroundColor: "#004FFE",
        }}
      >
        <View style={{ flex: 0.3 }}>
          <Image
            source={doctor_image ? { uri: `${doctor_image}` } : profilepc}
            style={{ width: 80, height: 80, alignSelf: "center" }}
          />
        </View>

        <TouchableOpacity
          onPress={() => alert(title)}
          style={{ flex: 0.6, justifyContent: "center" }}
        >
          <Text style={{ fontSize: 20, color: "#fff", fontWeight: "bold" }}>
            {doctor_name}
          </Text>
          <Text style={{ color: "#6164F0", fontWeight: "bold" }}>4.5</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => console.log(title[0])}
          style={{ flex: 0.1, alignItems: "center" }}
        >
          <FontAwesome5 active size={20} name={"trash-alt"} color="#fff" />
        </TouchableOpacity>
      </View>
      <View style={{ height: 20, width: "100%" }} />
    </View>
  );
};

const SavedList = ({ navigation }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [doctors, setDoctors] = useState([]);
  const id = navigation.getParam("id");
  const title = navigation.getParam("title");
  const token = navigation.getParam("token");

  useEffect(() => {
    getFolderDoctors();
  }, []);

  let data = [
    {
      equipo: "Rr.Roaa ",
      nombre: "Last Massage",
    },
    {
      equipo: "Dr.Khaled",
      nombre: "Last Massage",
    },
    {
      equipo: "Marah",
      nombre: "Last Massage",
    },
  ];

  const getFolderDoctors = () => {
    axios
      .get(`https://nextstageksa.com/clinic/api/favorite/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        setDoctors([...response.data.data]);
        Alert.alert("Add to Collection", "You a Collection Succussfully");
      })
      .catch((error) => {
        console.log("addToCollection error", error);
      });
  };

  return (
    <View style={{ flex: 1 }}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <TouchableOpacity
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(false)}
            >
              <Text style={styles.textStyle}> Delete </Text>
            </TouchableOpacity>
            <View style={{ height: 10 }} />
            <View style={{ borderBottomWidth: 1, width: "90%" }} />
            <View style={{ height: 10 }} />
            <TouchableOpacity
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(false)}
            >
              <Text style={styles.textStyle}> Report </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

      <View
        style={{
          justifyContent: "center",
          backgroundColor: "#004FFE",
          height: "12%",
          borderBottomEndRadius: 20,
          borderBottomLeftRadius: 20,
        }}
      >
        <View
          style={{
            marginTop: 20,
            marginRight: 20,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ flex: 0.1 }}
          >
            <FontAwesome5 active size={30} name={"angle-left"} color="white" />
          </TouchableOpacity>

          <View
            style={{
              flex: 0.8,
              alignItems: "center",
              justifyContent: "center",
              paddingRight: 20,
            }}
          >
            <Text style={{ color: "#fff", fontSize: 23 }}>{title}</Text>
          </View>
        </View>
      </View>

      <View style={{ flex: 10 / 9, marginTop: "7%" }}>
        <FlatList
          data={doctors}
          renderItem={({ item }) => (
            <Docotor
              doctor_id={item.doctor_id}
              doctor_name={item.doctor_name}
              doctor_image={item.doctor_image}
            />
          )}
          keyExtractor={(item) => item.doctor_id}
        />
      </View>
    </View>
  );
};

export default SavedList;

SavedList.navigationOptions = () => {
  return {
    header: () => false,
  };
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    width: "70%",
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 10,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#004FFE",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 20,
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    fontSize: 20,
  },
});

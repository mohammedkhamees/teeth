import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import logo from "../../../assets/images/logo.png";
import styles from "./style";

const Welcome = ({ navigation }) => {
  const login = () => navigation.navigate("Login");
  const register = () => navigation.navigate("FirstRegister");

  return (
    <View style={styles.container}>
      <View style={styles.imgContainer}>
        <Image source={logo} style={styles.img} />
      </View>

      <View style={styles.btnContainer}>
        <TouchableOpacity onPress={login} style={styles.btn}>
          <Text style={styles.btnText}>Login</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={register} style={styles.btn}>
          <Text style={styles.btnText}>Sign Up</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Welcome;

Welcome.navigationOptions = () => {
  return {
    header: () => false,
  };
};

import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#407CFF",
    justifyContent: "center",
    alignItems: "center",
  },

  imgContainer: { alignSelf: "center", marginTop: 15 },

  img: { width: 300, height: 140 },

  btnContainer: {
    marginTop: "50%",
  },

  btn: {
    alignItems: "center",
    backgroundColor: "white",
    padding: 10,
    borderWidth: 2,
    borderRadius: 28,
    borderColor: "white",
    marginTop: 20,
  },

  btnText: {
    color: "#777A79",
    fontSize: 25,
    fontWeight: "bold",
    paddingLeft: 50,
    paddingRight: 50,
  },
});

export default styles;

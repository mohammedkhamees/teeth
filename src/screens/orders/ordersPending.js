// @refresh reset

import React, { useEffect, useState } from "react";
import {
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  Text,
  FlatList,
  Dimensions,
  Alert,
} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import profilepc from "../../../assets/images/profilepc.png";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";

const Product = ({ equipo, nombre, deleteOrder, id }) => {
  return (
    <View>
      <View
        style={{
          height: 100,
          width: "100%",
          flexDirection: "row",
          alignItems: "center",
          backgroundColor: "#004FFE",
        }}
      >
        <View style={{ flex: 0.3 }}>
          <Image
            source={profilepc}
            // source={{
            //   uri: nombre,
            // }}
            style={{ width: 80, height: 80, alignSelf: "center" }}
          />
        </View>

        <View style={{ flex: 0.6, justifyContent: "center" }}>
          <Text style={{ fontSize: 20, color: "#fff", fontWeight: "bold" }}>
            {equipo}
          </Text>
        </View>

        <TouchableOpacity
          style={{ flex: 0.1, alignItems: "center" }}
          onPress={() => deleteOrder(id)}
        >
          <FontAwesome5 active size={20} name={"trash-alt"} color="white" />
        </TouchableOpacity>
      </View>
      <View style={{ height: 20, width: "100%" }} />
    </View>
  );
};

const OrdersPending = ({ navigation }) => {
  const [orders, setOrders] = useState([]);
  const [token, setToken] = useState("");

  useEffect(() => {
    getToken();
  }, []);

  const getOrders = (token) => {
    axios
      .get(
        `https://nextstageksa.com/clinic/api/order/buyer/all?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        setOrders([
          ...response.data.data.filter((order) => order.approval === 0),
        ]);
      })
      .catch((error) => {
        console.log("getOrders error", error);
      });
  };

  const getToken = async () => {
    const token = JSON.parse(await AsyncStorage.getItem("token"));
    if (token) {
      setToken(token);
      getOrders(token);
    } else {
      console.log("ERROR GET TOKEN");
    }
  };

  const deleteOrder = (id) => {
    setOrders([...orders.filter((order) => order.order_id !== id)]);
    axios
      .get(
        `https://nextstageksa.com/clinic/api/order/${id}/buyer/CancelOrder?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        // Alert.alert("Delete", "Order has been Deleted Succussfully");
      })
      .catch((error) => {
        console.log("error", error);
      });
  };

  return (
    <View style={{ flex: 1 }}>
      <View
        style={{
          justifyContent: "center",
          backgroundColor: "#004FFE",
          height: "12%",
          borderBottomEndRadius: 20,
          borderBottomLeftRadius: 20,
        }}
      >
        <View
          style={{
            marginTop: 20,
            marginRight: 20,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ flex: 0.1 }}
          >
            <FontAwesome5 active size={30} name={"angle-left"} color="white" />
          </TouchableOpacity>

          <View style={{ flex: 0.8, alignItems: "center" }}>
            <Text style={{ color: "#fff", fontSize: 20 }}>Orders</Text>
          </View>

          <View
            styles={{
              flex: 0.3,
              backgroundColor: "yellow",
              width: "100%",
              height: "100%",
            }}
          ></View>
        </View>
      </View>

      <View style={{ flexDirection: "row", padding: 25 }}>
        <TouchableOpacity
          onPress={() => navigation.navigate("OrdersPending")}
          style={{ flex: 0.5, justifyContent: "center", alignItems: "center" }}
        >
          <Text style={{ color: "#004FFE", fontSize: 22 }}>Pending</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => navigation.navigate("OrdersConfirm")}
          style={{ flex: 0.5, justifyContent: "center", alignItems: "center" }}
        >
          <Text style={{ color: "#6164F0", fontSize: 22 }}>Confirmed</Text>
        </TouchableOpacity>
      </View>
      <View style={{ height: 15, width: "100%" }} />

      <View style={{ flex: 10 / 9 }}>
        <FlatList
          data={orders}
          renderItem={({ item }) => (
            <Product
              equipo={item.medicine_name}
              nombre={item.image}
              id={item.order_id}
              deleteOrder={deleteOrder}
            />
          )}
          keyExtractor={(item) => `${item.order_id}`}
        />
      </View>
    </View>
  );
};

export default OrdersPending;

OrdersPending.navigationOptions = () => {
  return {
    header: () => false,
  };
};

import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  Text,
  FlatList,
  Dimensions,
  Alert,
} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { RadioButton } from "react-native-paper";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";

const Report = ({ navigation }) => {
  const [problem, setProblem] = useState("");
  const [token, setToken] = useState("");

  useEffect(() => {
    getToken();
  }, []);

  const getToken = async () => {
    const token = JSON.parse(await AsyncStorage.getItem("token"));
    if (token) {
      setToken(token);
    } else {
      console.log("ERROR GET TOKEN");
    }
  };

  const submitReport = () => {
    axios
      .post(
        `https://nextstageksa.com/clinic/api/ReportUser?key=6Q4AE52F15F8B82735E9485CA3EC7`,
        { reason: problem },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        console.log(response.data);
        Alert.alert("Thanks, we will review you report soon.");
      })
      .catch((error) => {
        console.log("submitReport ERROR : ", error);
      });
  };

  return (
    <View style={{ flex: 1 }}>
      <View
        style={{
          justifyContent: "center",
          backgroundColor: "#004FFE",
          height: "12%",
          borderBottomEndRadius: 20,
          borderBottomLeftRadius: 20,
        }}
      >
        <View
          style={{
            marginTop: 20,
            marginRight: 20,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <View
            styles={{
              flex: 0.3,
              backgroundColor: "yellow",
              width: "100%",
              height: "100%",
            }}
          ></View>

          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ flex: 0.1 }}
          >
            <FontAwesome5 active size={30} name={"angle-left"} color="white" />
          </TouchableOpacity>

          <View style={{ flex: 0.8, alignItems: "center" }}>
            <Text style={{ color: "#fff", fontSize: 20 }}> Report</Text>
          </View>
        </View>
      </View>

      <View style={{ height: "10%" }} />

      <TextInput
        style={{
          width: "85%",
          height: 200,
          borderRadius: 30,
          borderWidth: 1,
          borderColor: "#969698",
          alignItems: "center",
          padding: 20,
          alignSelf: "center",
        }}
        multiline={true}
        numberOfLines={6}
        onChangeText={setProblem}
      />

      <View style={{ height: "3%" }} />

      <View style={{ flexDirection: "row", marginLeft: 30 }}>
        <View style={{ flex: 0.5, padding: 20 }}>
          <View
            style={{ flexDirection: "row", alignItems: "center", padding: 5 }}
          >
            <View
              style={{
                borderWidth: Platform.OS === "ios" ? 1 : 0,
                borderRadius: 22,
                borderColor: "#969698",
              }}
            >
              <RadioButton
                value="one"
                status={problem === "one" ? "checked" : "unchecked"}
                onPress={() => setProblem("one")}
                color="#969698"
              />
            </View>
            <View>
              <Text style={{ color: "#969698" }}> mustafa </Text>
            </View>
          </View>

          <View
            style={{ flexDirection: "row", alignItems: "center", padding: 5 }}
          >
            <View
              style={{
                borderWidth: Platform.OS === "ios" ? 1 : 0,
                borderRadius: 22,
                borderColor: "#969698",
              }}
            >
              <RadioButton
                value="two"
                status={problem === "two" ? "checked" : "unchecked"}
                onPress={() => setProblem("two")}
                color="#969698"
              />
            </View>
            <View>
              <Text style={{ color: "#969698" }}> mustafa </Text>
            </View>
          </View>

          <View
            style={{ flexDirection: "row", alignItems: "center", padding: 5 }}
          >
            <View
              style={{
                borderWidth: Platform.OS === "ios" ? 1 : 0,
                borderRadius: 22,
                borderColor: "#969698",
              }}
            >
              <RadioButton
                value="three"
                status={problem === "three" ? "checked" : "unchecked"}
                onPress={() => setProblem("three")}
                color="#969698"
              />
            </View>
            <View>
              <Text style={{ color: "#969698" }}> mustafa </Text>
            </View>
          </View>

          <View
            style={{ flexDirection: "row", alignItems: "center", padding: 5 }}
          >
            <View
              style={{
                borderWidth: Platform.OS === "ios" ? 1 : 0,
                borderRadius: 22,
                borderColor: "#969698",
              }}
            >
              <RadioButton
                value="four"
                status={problem === "four" ? "checked" : "unchecked"}
                onPress={() => setProblem("four")}
                color="#969698"
              />
            </View>
            <View>
              <Text style={{ color: "#969698" }}> mustafa </Text>
            </View>
          </View>
        </View>

        <View style={{ flex: 0.5 }}></View>
      </View>

      <View style={{ height: "7%" }} />

      <View style={{ height: 22 }} />

      <TouchableOpacity
        style={{
          borderRadius: 40,
          backgroundColor: "#004FFE",
          width: "50%",
          alignSelf: "center",
          height: 40,
          justifyContent: "center",
        }}
        onPress={submitReport}
      >
        <Text style={{ alignSelf: "center", color: "#fff" }}> Report </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Report;

Report.navigationOptions = () => {
  return {
    header: () => false,
  };
};

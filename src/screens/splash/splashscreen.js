import React from "react";
import { View, Image } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { NavigationActions, StackActions } from "react-navigation";

import styles from "./styles";

const loggedIn = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: "BottomTab" })],
  //actions: [NavigationActions.navigate({ routeName: "BottomTab" })],
});

const notLoggedIn = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: "Login" })],
  //actions: [NavigationActions.navigate({ routeName: "MainPage" })],
});

export default class SplashScreen extends React.Component {
  render() {
    AsyncStorage.getItem("api_token").then((value) => {
      if (value) {
        setTimeout(() => {
          this.props.navigation.dispatch(loggedIn);
        }, 3600);
      } else {
        setTimeout(() => {
          this.props.navigation.dispatch(notLoggedIn);
        }, 3600);
      }
    });
    return (
      <View style={styles.container}>
        <Image
          source={require("../../../assets/gif/Splash.gif")}
          //resizeMode={"contain"}
          style={styles.splash}
        />
      </View>
    );
  }
}
